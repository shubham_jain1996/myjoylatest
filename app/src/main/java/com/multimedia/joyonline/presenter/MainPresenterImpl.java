package com.multimedia.joyonline.presenter;

import com.multimedia.joyonline.model.model.fragmentData.MainModelFragment;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;

import java.util.List;

public class MainPresenterImpl implements MainPresenter, GetNavigationItem.OnFinishedListener {

    private MainView mainView;
    private GetNavigationItem getQuoteInteractor;

    public MainPresenterImpl(MainView mainView, GetNavigationItem getQuoteInteractor) {
        this.mainView = mainView;
        this.getQuoteInteractor = getQuoteInteractor;
    }

    @Override
    public void onButtonClick() {


        getQuoteInteractor.getNavigationItem(this);

    }

    @Override
    public void onDestroy() {
        mainView = null;
    }

    @Override
    public void GetData() {

        getQuoteInteractor.getNavigationItem(this);

    }

    @Override
    public void sideMenuListItem(List<SideMenuList> sideMenuData) {
        mainView.setSideMenuList(sideMenuData);
    }
}
