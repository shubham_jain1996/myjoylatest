package com.multimedia.joyonline.presenter;

import com.multimedia.joyonline.model.model.fragmentData.MainModelFragment;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;

import java.util.List;

public interface GetNavigationItem {
    interface OnFinishedListener {
        void sideMenuListItem(List<SideMenuList> sideMenuData);
    }

    void getNavigationItem(OnFinishedListener listener);
}
