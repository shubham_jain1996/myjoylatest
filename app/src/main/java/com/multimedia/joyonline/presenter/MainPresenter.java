package com.multimedia.joyonline.presenter;

public interface  MainPresenter {
    void onButtonClick();

    void onDestroy();

    void GetData();
}
