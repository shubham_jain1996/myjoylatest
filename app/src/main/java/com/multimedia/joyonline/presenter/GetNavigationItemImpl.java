package com.multimedia.joyonline.presenter;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.fragmentData.MainModelFragment;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;
import com.multimedia.joyonline.model.network.DataService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetNavigationItemImpl  implements GetNavigationItem{

    private List<SideMenuList> sideListData=new ArrayList<>();



    private  List<SideMenuList> getSideListData(){
        sideListData.add(new SideMenuList("Home",R.drawable.ic_home,0,false));
     //   sideListData.add(new SideMenuList("News",R.drawable.ic_news,32,false));
      /*  sideListData.add(new SideMenuList("Politics",R.drawable.ic_politics,40,false));
        sideListData.add(new SideMenuList("Business",R.drawable.ic_business,18,false));
        sideListData.add(new SideMenuList("Entertainment",R.drawable.ic_entertainment,19,false));
        sideListData.add(new SideMenuList("Sports",R.drawable.ic_sports,46,false));
        sideListData.add(new SideMenuList("Opinion",R.drawable.ic_opinion,37,false));
        sideListData.add(new SideMenuList("Lifestyle",R.drawable.ic_lifestyle,28,false));
        sideListData.add(new SideMenuList("Infographic",R.drawable.ic_infographic,26,false));
        sideListData.add(new SideMenuList("World",R.drawable.ic_world,52,false));
        sideListData.add(new SideMenuList("Document",R.drawable.ic_document,52,false));
        sideListData.add(new SideMenuList("Media",R.drawable.ic_media,52,false));
        sideListData.add(new SideMenuList("Setting",R.drawable.ic_setting,52,false));
        sideListData.add(new SideMenuList("More",R.drawable.ic_more,1,false));*/
        return sideListData;
    }


    @Override
    public void getNavigationItem(OnFinishedListener listener) {
        listener.sideMenuListItem(getSideListData());

    }

}
