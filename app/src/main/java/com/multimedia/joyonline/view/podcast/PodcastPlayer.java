package com.multimedia.joyonline.view.podcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.PodCast_Player_Adapter;
import com.multimedia.joyonline.model.adapter.Popular_Podcast_Adapter;
import com.multimedia.joyonline.model.model.podcast.podCastPlayer.PodCast_MainModel;
import com.multimedia.joyonline.model.model.podcast.podCastPlayer.PodastModel;
import com.multimedia.joyonline.model.network.DataService;
import com.multimedia.joyonline.model.player.PlayerNotification;
import com.multimedia.joyonline.model.player.SharedExoPlayer;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.model.utility.RecyclerTouchListener;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.YouTubeVideos;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.multimedia.joyonline.model.utility.Util.stringForTime;

public class PodcastPlayer extends AppCompatActivity implements SharedExoPlayer.SharedExoPlayerListner {

    @BindView(R.id.radioChannel)
    RecyclerView radioChannel;
    private PodCast_Player_Adapter podAdapter;
    List<PodastModel> PlayerEpisodeList = new ArrayList<>();
    PodastModel selectedModel;
    String  episodeId;
    int pageNo=0;
    private boolean isplayOrNot;
    private boolean playerState;
    private boolean isPlaying;

    @BindView(R.id.seekbar)
    SeekBar seekPlayerProgress;
    @BindView(R.id.play)
    ImageButton play;

    @BindView(R.id.time)
    TextView startTime;
    private Handler handler;

    private boolean isLoading=false;
    boolean isNextAvailable=true;

    @BindView(R.id.progress_view)
    CircularProgressView progressView;
    private boolean isFirst=true;

    @BindView(R.id.channelName)
    TextView channelName;
    private int recyclerPosition;

    private InterstitialAd adView;
    private AdView mAdView;

    @BindView(R.id.CircularPrograssView)
    CircularProgressView progress_View;

    @BindView(R.id.channelImage)
    ImageView channelImage;

    @BindView(R.id.mainLayout)
    RelativeLayout mainLayout;

    String imageUrl;
    private AudioManager audioManager;
    private boolean isMute=false;

    @BindView(R.id.volume)
    ImageButton volume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podcast_player);
        ButterKnife.bind(this);
        mainLayout.setVisibility(View.GONE);
        SharedExoPlayer.getInstance().addItemListner(this);
        PlayerNotification.setupView();
        SharedExoPlayer.getInstance().checkAudio();
        episodeId=getIntent().getStringExtra("episodeId");
        imageUrl= getIntent().getStringExtra("image");
        setImageInView();
        setUprecyclerView();
        getPodCastPlayerList();
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();

    }

    private void setImageInView() {
        Picasso.get().load(imageUrl).resize(150,150).into(channelImage, new Callback() {
            @Override
            public void onSuccess() {
                progress_View.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(Exception e) {
                progress_View.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void setupMusicPLayer() {
    if (SharedExoPlayer.getInstance().exoPlayer==null){
        playSongOnClick(0);

    }
    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if ( MainActivity.adCounter %5 == 0){
                    adView.show();
                }
                MainActivity.adCounter++;
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                adView.loadAd(new AdRequest.Builder().build());
            }
        });
    }
    private void showAd(){
        MainActivity.adCounter+=1;
        if ( MainActivity.adCounter %5 == 0 && adView.isLoaded()){
            adView.show();
        }
    }


    private void getPodCastPlayerList() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<PodCast_MainModel>() {
            @Override
            public void onDataSuccess(PodCast_MainModel response) {
                if (response!=null && response.getData().size()>0){
                    int size= PlayerEpisodeList.size();
                    PlayerEpisodeList.addAll(response.getData());
                    podAdapter.notifyItemInserted(size);
                    isLoading=false;
                    if (response.getLinks().getNext()==null){
                        isNextAvailable=false;
                    }
                    if (isFirst){
                        mainLayout.setVisibility(View.VISIBLE);
                        setupMusicPLayer();
                    }
                  //  podAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getPodCastPlayerList(Application.shardPref.getString(Constants.Pref.TOKEN,""),String.valueOf(pageNo),episodeId);
    }

    private void setUprecyclerView() {
        radioChannel.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        radioChannel.setLayoutManager(lm);
        podAdapter = new PodCast_Player_Adapter(this,PlayerEpisodeList);
        radioChannel.setAdapter(podAdapter);
        radioChannel.addOnItemTouchListener(new RecyclerTouchListener(this, radioChannel, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                recyclerPosition=position1;
                playSongOnClick(position1);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        radioChannel.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager=LinearLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;

                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isLoading) {
                        if (isNextAvailable) {
                            pageNo++;
                            getPodCastPlayerList();
                        }else{
                            Toast.makeText(PodcastPlayer.this, "No more videos", Toast.LENGTH_SHORT).show();
                        }
                    }
                    isLoading=true;

                }
            }
        });
    }

    private void playSongOnClick(int position1) {
        showAd();
        play.setVisibility(View.VISIBLE);
        if (selectedModel!=null){
            selectedModel.setIsplay(false);
        }
        selectedModel=PlayerEpisodeList.get(position1);
        selectedModel.setIsplay(true);
        podAdapter.notifyDataSetChanged();
        SharedExoPlayer.getInstance().PlayAudio(PlayerEpisodeList.get(position1).getEnclosure().getUrl(),PlayerEpisodeList.get(position1).getTitle(),imageUrl);
        channelName.setText(SharedExoPlayer.getInstance().audio_name);
        isFirst=false;
    }

    @OnClick(R.id.backButton)
    public void ClickBackButton(){
        finish();
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        initSeekBar();
        switch (playbackState) {
            case Player.STATE_ENDED:
                setPlayPause(!isPlaying);
                SharedExoPlayer.getInstance().exoPlayer.seekTo(0);
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;
            case Player.STATE_READY:
                showPlayButton(true);
                if (SharedExoPlayer.getInstance().exoPlayer!=null){
                    playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();

                }
                if(playerState)
                {
                    setPlayPause(false);
                    isPlaying=true;
                }
                else
                {
                    setPlayPause(true);
                    isPlaying=false;
                }
                setProgress();
                if ( SharedExoPlayer.getInstance().exoPlayer!=null) {
                    PlayerNotification.mStateBuilder.setState(playWhenReady ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }
                break;
            case Player.STATE_BUFFERING:
                try {
                    showPlayButton(false);
                    PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_BUFFERING,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }catch (Exception e ){

                }

                break;
            case Player.STATE_IDLE:
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_NONE,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;

        }
        PlayerNotification.mMediaSession.setPlaybackState(PlayerNotification.mStateBuilder.build());
        //PlayerNotification.showNotification(PlayerNotification.mStateBuilder.build(),songPositionclick,radioData);
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }
    private void setPlayPause(boolean playerState) {
        isplayOrNot=playerState;
        if(!isplayOrNot){
            play.setImageResource(R.drawable.exo_icon_pause);
        }else{
            setProgress();
            play.setImageResource(R.drawable.exo_icon_play);
        }


    }
    public void showPlayButton(boolean b) {
        if (b){
            play.setVisibility(View.VISIBLE);
           progressView.setVisibility(View.INVISIBLE);
        }else {
            play.setVisibility(View.INVISIBLE);
            progressView.setVisibility(View.VISIBLE);
        }
    }
    private void setProgress() {
        try {
            if (seekPlayerProgress != null) {
              //  seekPlayerProgress.setProgress(0);
                seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration() / 1000);
            }
        }catch (Exception e){

        }
        if (SharedExoPlayer.getInstance().exoPlayer!=null) {
            String setTime= stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration());
            setTime=stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition())+" / "+setTime;
            startTime.setText(setTime);


        }

        if(handler == null)
            handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (SharedExoPlayer.getInstance().exoPlayer != null && isPlaying) {
                    seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()/1000);
                    int mCurrentPosition = (int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition() / 1000;
                    seekPlayerProgress.setProgress(mCurrentPosition);
                    String setTime= stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration());
                    setTime=stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition())+" / "+setTime;
                    startTime.setText(setTime);
                    handler.postDelayed(this, 1000);
                }
            }


        });
        handler.removeCallbacks(null);
    }
    private void initSeekBar() {

        seekPlayerProgress.requestFocus();

        seekPlayerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    // We're not interested in programmatically generated changes to
                    // the progress bar's position.
                    return;
                }
                SharedExoPlayer.getInstance().exoPlayer.seekTo(progress*1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekPlayerProgress.setMax(0);

    }

    @OnClick(R.id.lastEpisode)
    public void priviousChannel(){
        if (recyclerPosition<=0){
            return;
        }else{
            recyclerPosition--;
            if (recyclerPosition>=0 && recyclerPosition<PlayerEpisodeList.size()){
                playSongOnClick(recyclerPosition);
            }
        }


    }

    @OnClick(R.id.nextEpisode)
    public void forwordChannel(){
        if (recyclerPosition>=PlayerEpisodeList.size()){
            return;
        }else{
            recyclerPosition++;
            if (recyclerPosition<PlayerEpisodeList.size()){
                playSongOnClick(recyclerPosition);
            }
        }

    }

    @OnClick(R.id.stop)
    public void stopPlayer(){
        if (selectedModel!=null) {
            selectedModel.setIsplay(false);
            podAdapter.notifyDataSetChanged();
        }
        SharedExoPlayer.getInstance().endRadio();
        play.setVisibility(View.INVISIBLE);
      //  SharedExoPlayer.getInstance().addItemListner(null);
    }

    @OnClick(R.id.backButton)
    public void backPressed(){
    onBackPressed();
    }

    @OnClick(R.id.play)
    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
            selectedModel.setIsplay(false);
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
            selectedModel.setIsplay(true);
        }
        podAdapter.notifyDataSetChanged();
        setPlayPause(playerState);
    }
    @OnClick(R.id.volume)
    public void setMuteorUnmute(){
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (!isMute){
            volume.setImageResource(R.drawable.mute);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
            isMute=true;
        }else {
            volume.setImageResource(R.drawable.ic_volume);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            isMute=false;
        }
    }

}
