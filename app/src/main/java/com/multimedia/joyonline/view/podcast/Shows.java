package com.multimedia.joyonline.view.podcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.PodcastChannelAdapter;
import com.multimedia.joyonline.model.adapter.Popular_Podcast_Adapter;
import com.multimedia.joyonline.model.model.podcast.ShowItem;
import com.multimedia.joyonline.model.model.podcast.ShowsModel;
import com.multimedia.joyonline.model.network.DataService;
import com.multimedia.joyonline.model.player.SharedExoPlayer;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.model.utility.RecyclerTouchListener;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.multimedia.joyonline.model.utility.Util.stringForTime;

public class Shows extends AppCompatActivity {
    @BindView(R.id.podcastChannel)
    RecyclerView podcastChannel;
    private Popular_Podcast_Adapter podAdapter;
    ShowsModel showsData;
    String token;
    String channeLid;
    List<ShowItem> showsItemData= new ArrayList<>();

    @BindView(R.id.startTime)
    TextView startTime;

    @BindView(R.id.endTime)
    TextView endTime;

    @BindView(R.id.seekbar)
    SeekBar seekPlayerProgress;
    private Handler handler;

    @BindView(R.id.playerLayout)
    RelativeLayout playerLayout;
    private boolean isplayOrNot;

    @BindView(R.id.playButton)
    ImageButton playButton;

    @BindView(R.id.channelname)
    TextView channelname;

    @BindView(R.id.volume)
    ImageButton volume;

    private AudioManager audioManager;
    private boolean isMute=false;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private InterstitialAd adView;
    private AdView mAdView;

    @BindView(R.id.podCastImage)
    ImageView podCastImage;

    @BindView(R.id.progress_view)
    CircularProgressView progressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shows);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        if (getIntent()!=null){
            channeLid=getIntent().getStringExtra("channelId");
        }
        token= Application.shardPref.getString(Constants.Pref.TOKEN,"");
        setUprecyclerView();
        getShowUsingChannelId();
        setUpToolBar();
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();

    }

    private void setUpToolBar() {
        ImageView back= toolbar.findViewById(R.id.backPressed);
        TextView title= toolbar.findViewById(R.id.title);
        title.setText("SHOWS");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if ( MainActivity.adCounter %5 == 0){
                    adView.show();
                }
                MainActivity.adCounter++;
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                adView.loadAd(new AdRequest.Builder().build());
            }
        });
    }
    private void showAd(){
        MainActivity.adCounter+=1;
        if ( MainActivity.adCounter %5 == 0 && adView.isLoaded()){
            adView.show();
        }
    }


    private void setMuicPlayerLayout() {
        if (SharedExoPlayer.getInstance().exoPlayer!=null){
            playerLayout.setVisibility(View.VISIBLE);
            boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
            channelname.setText(SharedExoPlayer.getInstance().audio_name);
            Picasso.get().load(SharedExoPlayer.getInstance().imageUrl).error(R.drawable.myjoylogo).into(podCastImage, new Callback() {
                @Override
                public void onSuccess() {
                    progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    progressView.setVisibility(View.GONE);
                }
            });
            setPlayPause(!playerState);
            initSeekBar();
            setProgress();


        }  else {
            playerLayout.setVisibility(View.GONE);
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        setMuicPlayerLayout();
    }


    private void getShowUsingChannelId() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<ShowsModel>() {
            @Override
            public void onDataSuccess(ShowsModel response) {
                if (response!=null && response.getItems().size()>0){
                    showsItemData.addAll(response.getItems());
                    podAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getShowsList(token,channeLid);
    }

    private void setUprecyclerView() {
        podcastChannel.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        podcastChannel.setLayoutManager(lm);
        podAdapter = new Popular_Podcast_Adapter(Shows.this,showsItemData);
        podcastChannel.setAdapter(podAdapter);
        podcastChannel.addOnItemTouchListener(new RecyclerTouchListener(this, podcastChannel, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                showAd();
                Intent i = new Intent(Shows.this,PodcastPlayer.class);
                i.putExtra("image",showsItemData.get(position1).getImage());
                i.putExtra("episodeId",String.valueOf(showsItemData.get(position1).getId()));
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void setProgress() {
        try {
            if (seekPlayerProgress != null) {
               // seekPlayerProgress.setProgress(0);
                seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration() / 1000);
            }
        }catch (Exception e){

        }
        if (SharedExoPlayer.getInstance().exoPlayer!=null) {
            startTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition()));
            endTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()));


        }

        if(handler == null)
            handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                //if (SharedExoPlayer.getInstance().exoPlayer != null && isPlaying) {
                if (SharedExoPlayer.getInstance().exoPlayer != null ) {
                    seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()/1000);
                    int mCurrentPosition = (int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition() / 1000;
                    seekPlayerProgress.setProgress(mCurrentPosition);
                    startTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition()));
                    endTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()));
                    handler.postDelayed(this, 1000);
                }
            }


        });
        handler.removeCallbacks(null);
    }
    @OnClick(R.id.playButton)
    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
        }
        setPlayPause(playerState);
    }
    private void setPlayPause(boolean playerState) {
        isplayOrNot = playerState;
        if (!isplayOrNot) {
            playButton.setImageResource(R.drawable.ic_youtube_pause);
        } else {
            setProgress();
            playButton.setImageResource(R.drawable.you_tube_play);
        }
    }
    private void initSeekBar() {

        seekPlayerProgress.requestFocus();

        seekPlayerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    // We're not interested in programmatically generated changes to
                    // the progress bar's position.
                    return;
                }
                SharedExoPlayer.getInstance().exoPlayer.seekTo(progress*1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekPlayerProgress.setMax(0);

    }
    @OnClick(R.id.volume)
    public void setMuteorUnmute(){
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (!isMute){
            volume.setImageResource(R.drawable.mute);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
            isMute=true;
        }else {
            volume.setImageResource(R.drawable.ic_volume);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            isMute=false;
        }
    }
}
