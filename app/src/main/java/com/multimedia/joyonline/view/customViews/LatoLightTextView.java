package com.multimedia.joyonline.view.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rajesh on 9/1/17.
 */

@SuppressLint("AppCompatCustomView")
public class LatoLightTextView extends TextView {
    public LatoLightTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoLightTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoLightTextView(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Lato-Thin.ttf");
        setTypeface(tf ,Typeface.NORMAL);

    }

}
