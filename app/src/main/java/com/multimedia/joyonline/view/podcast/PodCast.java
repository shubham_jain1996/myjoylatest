package com.multimedia.joyonline.view.podcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.PodcastChannelAdapter;
import com.multimedia.joyonline.model.adapter.Popular_Podcast_Adapter;
import com.multimedia.joyonline.model.adapter.RadioAdapter;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.model.podcast.CollectionList;
import com.multimedia.joyonline.model.model.podcast.TokenClass;
import com.multimedia.joyonline.model.network.DataService;
import com.multimedia.joyonline.model.player.SharedExoPlayer;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.model.utility.RecyclerTouchListener;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.multimedia.joyonline.model.utility.Util.stringForTime;

public class PodCast extends AppCompatActivity {
    @BindView(R.id.podcastChannel)
    RecyclerView postCastChannelList;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.seekbar)
    SeekBar seekPlayerProgress;
    private Handler handler;
    @BindView(R.id.startTime)
    TextView startTime;

    @BindView(R.id.endTime)
    TextView endTime;

    @BindView(R.id.playButton)
    ImageButton playButton;

    @BindView(R.id.channelname)
    TextView channelname;

    private InterstitialAd adView;
    private AdView mAdView;

    @BindView(R.id.progress_view)
    CircularProgressView progressView;


/*
    @BindView(R.id.episodeList)
    RecyclerView episodeList;*/
    private List<RadioModel> radioData=new ArrayList<>();
    private PodcastChannelAdapter mAdapter;
    private Popular_Podcast_Adapter podAdapter;
    List<CollectionList> collectionLists = new ArrayList<>();
    String token;
    private SharedPreferences.Editor session;

    @BindView(R.id.playerLayout)
    RelativeLayout playerLayout;
    private boolean isplayOrNot;
    private AudioManager audioManager;
    private boolean isMute=false;

    @BindView(R.id.volume)
    ImageButton volume;

    @BindView(R.id.podCastImage)
    ImageView podCastImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pod_cast);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        setUpModelData();
        setUpChannelRecyclerView();
        if (!Application.shardPref.contains(Constants.Pref.TOKEN)){
            generateToken();
        }else{
            token = Application.shardPref.getString(Constants.Pref.TOKEN,"");
            getCollectionData(token);

        }
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();



       // setUpEpisodeList();
        setUpToolBar();

    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                if ( MainActivity.adCounter %5 == 0){
                    adView.show();
                }
                MainActivity.adCounter++;
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                adView.loadAd(new AdRequest.Builder().build());
            }
        });
    }
    private void showAd(){
        MainActivity.adCounter+=1;
        if ( MainActivity.adCounter %5 == 0 && adView.isLoaded()){
            adView.show();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        setMuicPlayerLayout();
        showAd();
    }

    private void setMuicPlayerLayout() {
        if (SharedExoPlayer.getInstance().exoPlayer!=null){
            playerLayout.setVisibility(View.VISIBLE);
            boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
            channelname.setText(SharedExoPlayer.getInstance().audio_name);
            Picasso.get().load(SharedExoPlayer.getInstance().imageUrl).error(R.drawable.myjoylogo).into(podCastImage, new Callback() {
                @Override
                public void onSuccess() {
                    progressView.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    progressView.setVisibility(View.GONE);
                }
            });
            setPlayPause(!playerState);
            initSeekBar();
            setProgress();


        }  else {
            playerLayout.setVisibility(View.GONE);
        }
    }

    private void getCollectionData(String access_token) {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<List<CollectionList>>() {
            @Override
            public void onDataSuccess(List<CollectionList> response) {
                if (response!=null && response.size()>0){
                    collectionLists.addAll(response);
                    mAdapter.notifyDataSetChanged();
                    }else {
                    generateToken();
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getCollectionData(access_token);
    }

    private void generateToken() {
        DataService dataService = new DataService(this);
        dataService.setOnServerListener(new DataService.ServerResponseListner<TokenClass>() {
            @Override
            public void onDataSuccess(TokenClass response) {
                if (response!=null){
                    token=response.getTokenType()+" "+response.getAccessToken();
                    saveTokenInSession(token);
                    getCollectionData(token);
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.generateToken();
    }

    private void setUpToolBar() {
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("Podcast");
        ImageView backButton= toolbar.findViewById(R.id.backPressed);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

   /* private void setUpEpisodeList() {
        episodeList.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        episodeList.setLayoutManager(lm);
        podAdapter = new Popular_Podcast_Adapter(this,radioData);
        episodeList.setAdapter(podAdapter);
        episodeList.addOnItemTouchListener(new RecyclerTouchListener(this, episodeList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }*/

    private void setUpChannelRecyclerView() {
        postCastChannelList.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);
        postCastChannelList.setLayoutManager(lm);
        mAdapter = new PodcastChannelAdapter(this,collectionLists);
        postCastChannelList.setAdapter(mAdapter);
        postCastChannelList.addOnItemTouchListener(new RecyclerTouchListener(this, postCastChannelList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                MainActivity.adCounter++;
                Intent  i = new Intent(PodCast.this,Shows.class);
                i.putExtra("channelId",String.valueOf(collectionLists.get(position1).getId()));
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @OnClick(R.id.volume)
    public void setMuteorUnmute(){
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (!isMute){
            volume.setImageResource(R.drawable.mute);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
            isMute=true;
        }else {
            volume.setImageResource(R.drawable.ic_volume);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            isMute=false;
        }
    }

    private void setUpModelData() {
        radioData.add(new RadioModel(this.getResources().getString(R.string.joy_title),this.getResources().getString(R.string.joy_tagline),"http://mmg.streamguys1.com/JoyFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.ic_joyfm,false));
       radioData.add(new RadioModel(this.getResources().getString(R.string.asempa_title),this.getResources().getString(R.string.asempa_tagline),"http://mmg.streamguys1.com/AsempaFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.asempa_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.adom_title),this.getResources().getString(R.string.adom_tagline),"http://mmg.streamguys1.com/AdomFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.adom_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.nhyira_title),this.getResources().getString(R.string.nhyira_tagline),"http://mmg.streamguys1.com/NhyiraFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.nhyira_fm,false));

    }
    public void saveTokenInSession(String position){
        session= Application.shardPref.edit();
        session.putString(Constants.Pref.TOKEN,position );
        session.apply();
    }
    private void setProgress() {
        try {
            if (seekPlayerProgress != null) {
               // seekPlayerProgress.setProgress(0);
                seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration() / 1000);
            }
        }catch (Exception e){

        }
        if (SharedExoPlayer.getInstance().exoPlayer!=null) {
            startTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition()));
            endTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()));


        }

        if(handler == null)
            handler = new Handler();
        //Make sure you update Seekbar on UI thread
        handler.post(new Runnable() {
            @Override
            public void run() {
                //if (SharedExoPlayer.getInstance().exoPlayer != null && isPlaying) {
                if (SharedExoPlayer.getInstance().exoPlayer != null ) {
                    seekPlayerProgress.setMax((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()/1000);
                    int mCurrentPosition = (int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition() / 1000;
                    seekPlayerProgress.setProgress(mCurrentPosition);
                    startTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition()));
                    endTime.setText(stringForTime((int) SharedExoPlayer.getInstance().exoPlayer.getDuration()));
                    handler.postDelayed(this, 1000);
                }
            }


        });
        handler.removeCallbacks(null);
    }
    @OnClick(R.id.playButton)
    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
        }
        setPlayPause(playerState);
    }
    private void setPlayPause(boolean playerState) {
        isplayOrNot = playerState;
        if (!isplayOrNot) {
            playButton.setImageResource(R.drawable.ic_youtube_pause);
        } else {
            setProgress();
            playButton.setImageResource(R.drawable.you_tube_play);
        }
    }
    private void initSeekBar() {

        seekPlayerProgress.requestFocus();

        seekPlayerProgress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser) {
                    // We're not interested in programmatically generated changes to
                    // the progress bar's position.
                    return;
                }
                SharedExoPlayer.getInstance().exoPlayer.seekTo(progress*1000);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        seekPlayerProgress.setMax(0);

    }

    @Override
    public void onBackPressed() {
        SharedExoPlayer.getInstance().endRadio();
        SharedExoPlayer.getInstance().addItemListner(null);
        super.onBackPressed();
    }


}
