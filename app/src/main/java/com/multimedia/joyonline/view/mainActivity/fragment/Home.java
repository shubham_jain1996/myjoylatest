package com.multimedia.joyonline.view.mainActivity.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.CategoryAdapter;
import com.multimedia.joyonline.model.adapter.HomeDataAdapter;
import com.multimedia.joyonline.model.adapter.YouTubeVedioAdapter;
import com.multimedia.joyonline.model.adapter.fragmentDataAdapter;
import com.multimedia.joyonline.model.model.homeModel.Data;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;
import com.multimedia.joyonline.model.model.tv.YoutubeChannelList;
import com.multimedia.joyonline.model.model.youtubeFiles.HomeRecyclerModel;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;
import com.multimedia.joyonline.model.model.youtubeFiles.YoutubeVedioList;
import com.multimedia.joyonline.model.network.DataService;
import com.multimedia.joyonline.model.utility.DataLoadingInterfaceHome;
import com.multimedia.joyonline.model.utility.GetHomebackground;
import com.multimedia.joyonline.model.utility.InternetConnection;
import com.multimedia.joyonline.presenter.GetNavigationItemImpl;
import com.multimedia.joyonline.presenter.MainPresenter;
import com.multimedia.joyonline.presenter.MainPresenterImpl;
import com.multimedia.joyonline.presenter.MainView;
import com.multimedia.joyonline.view.dialoge.ProgressDialogue;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.YouTubeVideos;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Home extends Fragment implements DataLoadingInterfaceHome,YouTubePlayer.OnInitializedListener, MainActivity.playerOnFullScreen {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final int RECOVERY_REQUEST =1;
    LinearLayoutManager layoutManager;
    private MainPresenter presenter;
    private HomeDataAdapter mainAdapter;
    Data data;
    Data FragmentData;
    @BindView(R.id.fragmentData)
    RecyclerView fragmentData;

    @BindView(R.id.noInternetLayout)
    RelativeLayout noInternetLayout;

    @BindView(R.id.youtube_fragment)
    FrameLayout youtubelayout;

    boolean isFullScreen=false;



   YouTubePlayer youTubePlayer;

   String liveTvChannelId;





    

    private CategoryAdapter categoryAdapter;
    ProgressDialogue helpDialog;
    private List<PlayListModel> playListModelList=new ArrayList<>();
    List<HomeRecyclerModel> homeData = new ArrayList<>();
    private YouTubePlayerSupportFragment mYoutubePlayerFragment;
    private YouTubePlayerFragment youTubePlayerFragment;


    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view!=null){
            return view;
        }
        view= inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);
        helpDialog= new ProgressDialogue(getActivity());
        helpDialog.setCancelable(false);
        ((MainActivity)getActivity()).initializeInterface(this);
        setUpHomeData();
        setUpModelClassData();
        checkInternetAndGetData();
        setUpHomeData();



        return view;
    }

    private void getLiveTv_Id() {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeChannelList>() {
            @Override
            public void onDataSuccess(YoutubeChannelList response) {
                if (response!=null && response.getItems().size()>0){
                    liveTvChannelId=response.getItems().get(0).getId().getVideoId();
                    if (liveTvChannelId!=null && youTubePlayer!=null) {
                        youTubePlayer.loadVideo(liveTvChannelId);
                    }
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getMyJoyChannelId();
    }

    private void checkInternetAndGetData() {
        if (InternetConnection.checkConnection(getActivity())) {
            helpDialog.show();
            noInternetLayout.setVisibility(View.GONE);
            youtubelayout.setVisibility(View.VISIBLE);
            fragmentData.setVisibility(View.VISIBLE);
            initialiseYouTubePlayer();
            getLiveTv_Id();
            for (int i = 0; i < playListModelList.size(); i++) {
                PlayListModel data = playListModelList.get(i);
                //  new GetHomebackground(this).execute(data);
                getYouTubeVedios(data);
            }
        }else{
            if (helpDialog.isShowing()){
                helpDialog.dismiss();
            }
            noInternetLayout.setVisibility(View.VISIBLE);
            youtubelayout.setVisibility(View.INVISIBLE);
            fragmentData.setVisibility(View.INVISIBLE);
        }
    }

    @OnClick(R.id.retry)
    public void clickRetry(){
        checkInternetAndGetData();
    }

    private void initialiseYouTubePlayer() {
        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_fragment, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(getResources().getString(R.string.YouTubekey), this);


    }


    private void setUpHomeData() {
        fragmentData.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(getActivity(), 1);
        fragmentData.setLayoutManager(verticalLayoutManager);
        mainAdapter = new HomeDataAdapter(getActivity(), playListModelList);
        fragmentData.setAdapter(mainAdapter);
    }


    private void setUpModelClassData() {
        List<PlayListModel> playListModel = new ArrayList<>();
        playListModel.add(new PlayListModel("AM News"	,"PLEhaP2Z2iO3o9Il9c8uq72pWOrNlkv8bl","https://i.ytimg.com/vi/7G0ztHUvSu8/default.jpg"));
        playListModel.add(new PlayListModel("#JoySMS"	,"PLEhaP2Z2iO3qlggSdQHRJxXbc3QvoaN8I","https://i.ytimg.com/vi/lI6qr9jza4Q/default.jpg"));
        playListModel.add(new PlayListModel("AM Show NewsPaper Headlines"	,"PLEhaP2Z2iO3oqHIvPNByf1HI6MnXBCvMr","https://i.ytimg.com/vi/d_RVZyrJJso/default.jpg"));
        playListModel.add(new PlayListModel("AM Show"	,"PLEhaP2Z2iO3p7G1akrpLbz_0lHWiWBygW","https://i.ytimg.com/vi/2NOsmE_l0Qg/sddefault.jpg"));
        playListModel.add(new PlayListModel("AM Talk"	,"PLEhaP2Z2iO3qomGcf_L1_B0fqLyzWZoox","https://i.ytimg.com/vi/81aJh9XHTvY/sddefault.jpg"));
        playListModel.add(new PlayListModel("News Desk"	,"PLEhaP2Z2iO3qD3bb5GusCbIkc-4ENZYxE","https://i.ytimg.com/vi/F-HwA5d0Dew/default.jpg"));
        playListModel.add(new PlayListModel("Joy News Today (Midday News)"	,"PLEhaP2Z2iO3pxoUOfkOAl2Ovf6GeH7gaz","https://i.ytimg.com/vi/7G0ztHUvSu8/default.jpg"));
        playListModel.add(new PlayListModel("Joy News Prime"	,"PLEhaP2Z2iO3pYo-hVoxnzx4YJ8jTZ2Zaa","https://i.ytimg.com/vi/AfTVZJZL1zg/default.jpg"));
        playListModel.add(new PlayListModel("The Pulse"	,"PLEhaP2Z2iO3oYVQFMv3iA9ME5_8ej_hfg","https://i.ytimg.com/vi/QJ9OgJ9KS10/sddefault.jpg"));
        playListModel.add(new PlayListModel("Business Live"	,"PLEhaP2Z2iO3rCUbRjp95OaKI3szHQljb9","https://i.ytimg.com/vi/wmoJ22KFt14/default.jpg"));
        playListModel.add(new PlayListModel("PM Express"	,"PL806E0108960E607A","https://i.ytimg.com/vi/lf6UOew0f74/default.jpg"));
        playListModel.add(new PlayListModel("Joy News Interactive"	,"PLEhaP2Z2iO3qJnkOf4E7jl_0DJ5YxiEOX","https://i.ytimg.com/vi/lf6UOew0f74/default.jpg"));
        playListModel.add(new PlayListModel("Tech Talk"	,"PLEhaP2Z2iO3p5m__6E210EAiYxA50bBQA","https://i.ytimg.com/vi/Qc2GmoEYZmQ/sddefault.jpg"));
        playListModel.add(new PlayListModel("UPfront"	,"PLEhaP2Z2iO3oeWI47gZzjRJmk-4oKCZEa","https://i.ytimg.com/vi/-jFJxx-Aqj0/hqdefault.jpg"));
        playListModel.add(new PlayListModel("Executive Lounge"	,"PLEhaP2Z2iO3paS-YVFWbjDjOZd5IeiROA","https://i.ytimg.com/vi/bhDijWUi-es/default.jpg"));
        playListModel.add(new PlayListModel("News File"	,"PLB3FD32108C6F095D","https://i.ytimg.com/vi/msOuBOuc7MQ/default.jpg"));
        playListModel.add(new PlayListModel("Unscripted"	,"PLEhaP2Z2iO3ow0CyB2sbrQFpi47mn_TmC","https://i.ytimg.com/vi/5pVizGlrUYE/default.jpg"));
        playListModelList.addAll(playListModel);
    }


    private void getYouTubeVedios(PlayListModel playListModel) {
        DataService dataService = new DataService(getActivity());
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeVedioList>() {
            @Override
            public void onDataSuccess(YoutubeVedioList response) {
                if (response!=null){
                 //   homeData.add(new HomeRecyclerModel(title,playListId,response));
                    playListModel.setTopStories(response);
                    mainAdapter.notifyDataSetChanged();

                }
                if (helpDialog.isShowing()){
                    helpDialog.dismiss();
                }

            }

            @Override
            public void onDataFailiure() {
                if (helpDialog.isShowing()){
                    helpDialog.dismiss();
                }
            }
        });
        dataService.getYouTubeVedios4(playListModel.getPlayListId());
    }


    @Override
    public void preExecute() {

    }

    @Override
    public void postExecute(HomeRecyclerModel homeClasses) {
        homeData.add(homeClasses);
        mainAdapter.notifyDataSetChanged();
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        this.youTubePlayer=youTubePlayer;
        if (!b) {
            if (liveTvChannelId!=null) {
                youTubePlayer.loadVideo(liveTvChannelId);
            }
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
            youTubePlayer.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                @Override
                public void onFullscreen(boolean b) {
                    MainActivity.isFullScreen=b;

                }
            });


        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
         } else {
            String error = String.format("Error initializing YouTube player", errorReason.toString());
            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void fullScreenClick() {
        youTubePlayer.setFullscreen(false);
    }
}
