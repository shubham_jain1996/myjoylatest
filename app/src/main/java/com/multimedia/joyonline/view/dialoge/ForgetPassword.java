package com.multimedia.joyonline.view.dialoge;

import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.multimedia.joyonline.R;

public class ForgetPassword extends DialogFragment {
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.forgetpassword_dialogue, null);
        Button dialogButton =  dialogView.findViewById(R.id.submit);
        ImageView close =  dialogView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new AwesomeSuccessDialog(getActivity())
                        .setTitle("")
                        .setMessage(R.string.successText)
                        .setColoredCircle(R.color.colorPrimaryDark)
                        .setDialogIconAndColor(R.drawable.ic_done_24px, R.color.white)
                        .setCancelable(true)
                        .setDoneButtonText("Close")
                        .setDoneButtonbackgroundColor(R.color.colorPrimaryDark)
                        .setDoneButtonTextColor(R.color.white)
                        .setDoneButtonClick(new Closure() {
                            @Override
                            public void exec() {
                                dismiss();
                        }
                        })

                        .show();

            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        return builder.create();
    }
}
