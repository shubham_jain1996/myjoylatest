package com.multimedia.joyonline.view.mainActivity.fragment.registerFragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.view.dialoge.SuccessDialoge;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.PlayerActivity;
import com.multimedia.joyonline.view.mainActivity.SignInActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SignIn extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.login)
    Button login;

    public static SignIn newInstance(String param1, String param2) {
        SignIn fragment = new SignIn();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_sign_in, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @OnClick(R.id.login)
    public void openMainActivity(){
        Intent i = new Intent(getActivity(), MainActivity.class);
        startActivity(i);
        getActivity().finish();
    }

    @OnClick(R.id.forgetPassword)
    public void showDialoge(){
        SuccessDialoge helpDialog = new SuccessDialoge();
        helpDialog.show(getActivity().getSupportFragmentManager(), "forget");

    }

    @OnClick(R.id.signUp)
    public void openSignupPage(){
        ((SignInActivity)getActivity()).replaceFragmentWithBackStack(new SignUp(),"signUp");
    }


}
