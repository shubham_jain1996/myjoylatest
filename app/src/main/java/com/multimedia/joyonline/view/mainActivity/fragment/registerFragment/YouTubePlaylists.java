package com.multimedia.joyonline.view.mainActivity.fragment.registerFragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.PlaylistAdapter;
import com.multimedia.joyonline.model.adapter.RadioAdapter;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;
import com.multimedia.joyonline.model.utility.RecyclerTouchListener;
import com.multimedia.joyonline.view.mainActivity.YouTubeVideos;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class YouTubePlaylists extends Fragment {

    @BindView(R.id.playlists)
    RecyclerView playListsView;
    private LinearLayoutManager layoutManager;
    private PlaylistAdapter mAdapter;
    java.util.List<PlayListModel> playListModelList=new ArrayList<>();


    public static YouTubePlaylists newInstance(String param1, String param2) {
        YouTubePlaylists fragment = new YouTubePlaylists();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_you_tube_playlists, container, false);
        ButterKnife.bind(this,view);
        setUpModelClassData();
        setUpRecyclerView();




        return view;
    }

    private void setUpModelClassData() {
        List<PlayListModel> playListModel = new ArrayList<>();
        playListModel.add(new PlayListModel("AM News"	,"PLEhaP2Z2iO3o9Il9c8uq72pWOrNlkv8bl","https://i.ytimg.com/vi/7G0ztHUvSu8/default.jpg"));
        playListModel.add(new PlayListModel("#JoySMS"	,"PLEhaP2Z2iO3qlggSdQHRJxXbc3QvoaN8I","https://i.ytimg.com/vi/lI6qr9jza4Q/default.jpg"));
        playListModel.add(new PlayListModel("AM Show NewsPaper Headlines"	,"PLEhaP2Z2iO3oqHIvPNByf1HI6MnXBCvMr","https://i.ytimg.com/vi/d_RVZyrJJso/default.jpg"));
        playListModel.add(new PlayListModel("AM Show"	,"PLEhaP2Z2iO3p7G1akrpLbz_0lHWiWBygW","https://i.ytimg.com/vi/2NOsmE_l0Qg/sddefault.jpg"));
        playListModel.add(new PlayListModel("AM Talk"	,"PLEhaP2Z2iO3qomGcf_L1_B0fqLyzWZoox","https://i.ytimg.com/vi/81aJh9XHTvY/sddefault.jpg"));
        playListModel.add(new PlayListModel("News Desk"	,"PLEhaP2Z2iO3qD3bb5GusCbIkc-4ENZYxE","https://i.ytimg.com/vi/F-HwA5d0Dew/default.jpg"));
        playListModel.add(new PlayListModel("Joy News Today (Midday News)"	,"PLEhaP2Z2iO3pxoUOfkOAl2Ovf6GeH7gaz","https://i.ytimg.com/vi/7G0ztHUvSu8/default.jpg"));
        playListModel.add(new PlayListModel("Joy News Prime"	,"PLEhaP2Z2iO3pYo-hVoxnzx4YJ8jTZ2Zaa","https://i.ytimg.com/vi/AfTVZJZL1zg/default.jpg"));
        playListModel.add(new PlayListModel("The Pulse"	,"PLEhaP2Z2iO3oYVQFMv3iA9ME5_8ej_hfg","https://i.ytimg.com/vi/QJ9OgJ9KS10/sddefault.jpg"));
        playListModel.add(new PlayListModel("Business Live"	,"PLEhaP2Z2iO3rCUbRjp95OaKI3szHQljb9","https://i.ytimg.com/vi/wmoJ22KFt14/default.jpg"));
        playListModel.add(new PlayListModel("PM Express"	,"PL806E0108960E607A","https://i.ytimg.com/vi/lf6UOew0f74/default.jpg"));
        playListModel.add(new PlayListModel("Tech Talk"	,"PLEhaP2Z2iO3p5m__6E210EAiYxA50bBQA","https://i.ytimg.com/vi/Qc2GmoEYZmQ/sddefault.jpg"));
        playListModel.add(new PlayListModel("UPfront"	,"PLEhaP2Z2iO3oeWI47gZzjRJmk-4oKCZEa","https://i.ytimg.com/vi/-jFJxx-Aqj0/hqdefault.jpg"));
        playListModel.add(new PlayListModel("Executive Lounge"	,"PLEhaP2Z2iO3paS-YVFWbjDjOZd5IeiROA","https://i.ytimg.com/vi/bhDijWUi-es/default.jpg"));
        playListModel.add(new PlayListModel("News File"	,"PL17BAA07600A4F8CE","https://i.ytimg.com/vi/msOuBOuc7MQ/default.jpg"));
        playListModel.add(new PlayListModel("Unscripted"	,"PLEhaP2Z2iO3ow0CyB2sbrQFpi47mn_TmC","https://i.ytimg.com/vi/5pVizGlrUYE/default.jpg"));
        playListModelList.addAll(playListModel);
    }

    private void setUpRecyclerView() {
        playListsView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager lm = new GridLayoutManager(getActivity(), 2);
        playListsView.setLayoutManager(lm);
        mAdapter = new PlaylistAdapter(getActivity(),playListModelList);
        playListsView.setAdapter(mAdapter);
        playListsView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), playListsView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                Intent i = new Intent(getActivity(), YouTubeVideos.class);
                i.putExtra("playlistId",playListModelList.get(position1).getPlayListId());
                startActivity(i);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

}
