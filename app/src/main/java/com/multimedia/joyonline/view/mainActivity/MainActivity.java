package com.multimedia.joyonline.view.mainActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.os.Handler;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.NavigationAdapter;
import com.multimedia.joyonline.model.adapter.ViewPagerAdapter;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;
import com.multimedia.joyonline.model.player.NotiService;
import com.multimedia.joyonline.model.player.PlayerNotification;
import com.multimedia.joyonline.model.player.SharedExoPlayer;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.model.utility.InternetConnection;
import com.multimedia.joyonline.presenter.GetNavigationItemImpl;
import com.multimedia.joyonline.presenter.MainPresenter;
import com.multimedia.joyonline.presenter.MainPresenterImpl;
import com.multimedia.joyonline.presenter.MainView;
import com.multimedia.joyonline.view.dialoge.ProgressDialogue;
import com.multimedia.joyonline.view.mainActivity.fragment.Home;
import com.multimedia.joyonline.view.podcast.PodCast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, MainView, SharedExoPlayer.SharedExoPlayerListner {
    public static int adCounter;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DrawerLayout drawer;
    RecyclerView navigationItem;
    LinearLayoutManager layoutManager;
    MainPresenter presenter;
    private ViewPagerAdapter adapter;
    List<SideMenuList> categories = new ArrayList<>();
    private NavigationAdapter navigationAdapter;
    ProgressDialogue helpDialog;
    private int recyclerPosition;
    List<RadioModel> radioData= new ArrayList<>();
    private List<RadioModel> channelList=new ArrayList<>();
    private String songUrl;
    private int songPosition;
    private SharedPreferences.Editor session;
    private boolean doubleBackToExitPressedOnce=false;
    private AdView mAdView;
    private InterstitialAd adView;
    public static boolean isFullScreen=false;
    playerOnFullScreen playerOnFullScreen;
    String title;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        setUpModelData();
     /*   IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION.PLAY_ACTION);
        filter.addAction(Constants.ACTION.PRIVIOUS_ACTION);
        filter.addAction(Constants.ACTION.FORWORD_ACTION);
        registerReceiver(broadcastReceiver, filter);*/



         drawer = findViewById(R.id.drawer_layout);
        helpDialog= new ProgressDialogue(this);
        helpDialog.setCancelable(false);
        NavigationView navigationView = findViewById(R.id.navigation_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        navigationView.setNavigationItemSelectedListener(this);

        toolbar = findViewById(R.id.toolbar);
        tabLayout =  findViewById(R.id.tabs);
        LinearLayout linearLayout = (LinearLayout)tabLayout.getChildAt(0);
        linearLayout.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(R.color.white));
        drawable.setSize(1, 1);
        linearLayout.setDividerPadding(10);
        linearLayout.setDividerDrawable(drawable);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  getSupportActionBar().setHomeButtonEnabled(true);
      //  getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_24px);



        presenter = new MainPresenterImpl(this, new GetNavigationItemImpl());
        navigationItem = findViewById(R.id.navigationItem);
        navigationItem.setHasFixedSize(true);
        layoutManager=new LinearLayoutManager(this);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 1);
        navigationItem.setLayoutManager(lm);
        navigationAdapter=new NavigationAdapter(this,categories);
        navigationItem.setAdapter(navigationAdapter);
        viewPager = findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        presenter.GetData();
        adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
        setUpInterStialAd();
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager(),categories);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setVisibility(View.GONE);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
              /* if (adCounter%5==0) {
                   adView.show();
               }*/
            }
            @Override
            public void onAdFailedToLoad(int i) {
                Log.d("satyam", "onAdFailedToLoad: "+i);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }



    @Override
    public void setSideMenuList(List<SideMenuList> sideMenuList) {
        categories.addAll(sideMenuList);
        adapter.notifyDataSetChanged();
        navigationAdapter.notifyDataSetChanged();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolsearch, menu);

        return super.onCreateOptionsMenu(menu);



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.radio) {
            Intent i = new Intent(this, PlayerActivity.class);
            startActivity(i);
           // return true;
        }else if (item.getItemId() == R.id.podcast){
            Intent i = new Intent(this, PodCast.class);
            startActivity(i);
        }



        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_ENDED:
                //nextTrack();

                SharedExoPlayer.getInstance().exoPlayer.seekTo(0);
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;
            case Player.STATE_READY:
                if ( SharedExoPlayer.getInstance().exoPlayer!=null) {
                    PlayerNotification.mStateBuilder.setState(playWhenReady ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }
                break;
            case Player.STATE_BUFFERING:
                try {
                    PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_BUFFERING,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }catch (Exception e ){
                }

                break;
            case Player.STATE_IDLE:
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_NONE,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;

        }
        PlayerNotification.mMediaSession.setPlaybackState(PlayerNotification.mStateBuilder.build());
        PlayerNotification.showNotification(PlayerNotification.mStateBuilder.build(),songPosition,radioData);
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }
  /*  BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
             switch (intent.getAction()){
                case Constants.ACTION.PLAY_ACTION:
                    controlPlayer();
                    break;

                case Constants.ACTION.PRIVIOUS_ACTION:
                    priviousChannel();
                    break;

                case Constants.ACTION.FORWORD_ACTION:
                    forwordChannel();
                    break;
            }
        }
    };*/
    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
        }
       // setPlayPause(playerState);
    }
    public void priviousChannel(){
        recyclerPosition= Application.shardPref.getInt(Constants.Pref.PLAYERSTATE,0);
        if (recyclerPosition<=0){
            return;
        }else{
            recyclerPosition--;
            if (recyclerPosition>=0 && recyclerPosition<radioData.size()){
                saveStateInSession(recyclerPosition);
                playExoPlayerRadio(recyclerPosition,radioData);

            }
        }


    }
    public void forwordChannel(){
        recyclerPosition= Application.shardPref.getInt(Constants.Pref.PLAYERSTATE,0);
        if (recyclerPosition>=radioData.size()){
            return;
        }else{
            recyclerPosition++;
            if (recyclerPosition<radioData.size()){
                saveStateInSession(recyclerPosition);
                playExoPlayerRadio(recyclerPosition,radioData);
                PlayerActivity.songPositionclick=recyclerPosition;
            }
        }
    }
    private void setUpModelData() {
        radioData.add(new RadioModel(this.getResources().getString(R.string.joy_title),this.getResources().getString(R.string.joy_tagline),"http://mmg.streamguys1.com/JoyFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.ic_joyfm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.hitz_title),this.getResources().getString(R.string.hitz_tagline),"http://mmg.streamguys1.com/HitzFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.hitz_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.asempa_title),this.getResources().getString(R.string.asempa_tagline),"http://mmg.streamguys1.com/AsempaFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.asempa_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.adom_title),this.getResources().getString(R.string.adom_tagline),"http://mmg.streamguys1.com/AdomFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.adom_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.luv_title),this.getResources().getString(R.string.luv_tagline),"http://mmg.streamguys1.com/LuvFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.luv_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.nhyira_title),this.getResources().getString(R.string.nhyira_tagline),"http://mmg.streamguys1.com/NhyiraFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.nhyira_fm,false));

    }
    public void playExoPlayerRadio( int position,List<RadioModel> radioData){
        this.channelList=radioData;
        SharedExoPlayer.getInstance().PlayAudio(radioData.get(position).getSourceUrl(),radioData.get(position).getTitle(),"");
        this.songUrl=radioData.get(position).getSourceUrl();
        this.title=radioData.get(position).getTitle();
        songPosition=position;
        playMusic();
    }
    private void playMusic() {

        if(InternetConnection.checkConnection(this)) {
            if (SharedExoPlayer.getInstance().exoPlayer != null) {
                this.startService(new Intent(this, NotiService.class));
                   if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_READY) {
                        boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
                    } else if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                    }
                else {
                    SharedExoPlayer.getInstance().PlayAudio(songUrl,title,"");

                }
            } else {
                SharedExoPlayer.getInstance().PlayAudio(songUrl,title,"");

            }
        } else {
            InternetConnection.AlertBox(this, "Please Check Your Internet Connection");

        }


    }

    public void saveStateInSession(int position){
        session= Application.shardPref.edit();
        session.putInt(Constants.Pref.PLAYERSTATE,position );
        session.apply();
    }

    @Override
    public void onBackPressed() {
            if (isFullScreen){
                playerOnFullScreen.fullScreenClick();
            }else{
              doubleClickToExit();
            }

    }

    private void doubleClickToExit() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


    public interface playerOnFullScreen{
        void fullScreenClick();
    }

    public void initializeInterface(playerOnFullScreen callBack){
        this.playerOnFullScreen=callBack;
    }
}
