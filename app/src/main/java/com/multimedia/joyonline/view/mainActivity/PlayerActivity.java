package com.multimedia.joyonline.view.mainActivity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.media.session.MediaButtonReceiver;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.adapter.RadioAdapter;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.player.NotiService;
import com.multimedia.joyonline.model.player.PlayerNotification;
import com.multimedia.joyonline.model.player.SharedExoPlayer;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.model.utility.InternetConnection;
import com.multimedia.joyonline.model.utility.RecyclerTouchListener;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PlayerActivity extends AppCompatActivity implements SharedExoPlayer.SharedExoPlayerListner {

    private FragmentManager manager;
    private FragmentTransaction transaction;
    private boolean playerState;
    private boolean isPlaying=true;
    String songUrl;
    private int currentpos;
    int songPosition;

    List<RadioModel> channelList= new ArrayList<>();
    private NotificationManager notificationManager;
   // private AdView mAdView;
    List<RadioModel> radioData= new ArrayList<>();
    RadioModel radioModel;

    @BindView(R.id.radioChannel)
    RecyclerView radioChannel;

    @BindView(R.id.play)
    ImageView play;

    @BindView(R.id.progressbar)
    CircularProgressView progressbar;

    @BindView(R.id.volume)
    ImageView volume;


    @BindView(R.id.channelName)
    TextView channelName;

    @BindView(R.id.channelName1)
    TextView channelName1;

    @BindView(R.id.tagLine)
    TextView tagLineAbove;



    private LinearLayoutManager layoutManager;
    private RadioAdapter mAdapter;

    boolean isplayOrNot;
    int recyclerPosition=0;

    private AudioManager audioManager;
    private Handler handler;
    public boolean isMute=false;
    private SharedPreferences.Editor session;
    private boolean isback=true;
    static int songPositionclick;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
     private InterstitialAd adView;
    private AdView mAdView;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ButterKnife.bind(this);
        mAdView = findViewById(R.id.adView);
       AdRequest adRequest = new AdRequest.Builder().build();
       mAdView.loadAd(adRequest);
         notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        manager = getSupportFragmentManager();
        setUpModelData();
        setUpRecyclerView();
        setUpToolBar();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.ACTION.PLAY_ACTION);
        filter.addAction(Constants.ACTION.PRIVIOUS_ACTION);
        filter.addAction(Constants.ACTION.FORWORD_ACTION);
        registerReceiver(broadcastReceiver, filter);
        SharedExoPlayer.getInstance().addItemListner(this);
        PlayerNotification.setupView();
        SharedExoPlayer.getInstance().checkAudio();
        manageModelData(0,isback);
       adView = new InterstitialAd(this);
        adView.setAdUnitId(getResources().getString(R.string.interId));
            setUpInterStialAd();

    }

    private void setUpToolBar() {
        ImageView back= toolbar.findViewById(R.id.backPressed);
        TextView title= toolbar.findViewById(R.id.title);
        title.setText("Radio");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void setUpInterStialAd() {
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
               if ( MainActivity.adCounter %5 == 0){
                   adView.show();
               }
               MainActivity.adCounter++;
            }

            @Override
            public void onAdClosed() {
                super.onAdClosed();
                adView.loadAd(new AdRequest.Builder().build());
            }
        });
    }

    private void showAd(){
        MainActivity.adCounter+=1;
        if ( MainActivity.adCounter %5 == 0 && adView.isLoaded()){
            adView.show();
        }
    }
    private void setUpModelData() {
        radioData.add(new RadioModel(this.getResources().getString(R.string.joy_title),this.getResources().getString(R.string.joy_tagline),"http://mmg.streamguys1.com/JoyFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.ic_joyfm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.hitz_title),this.getResources().getString(R.string.hitz_tagline),"http://mmg.streamguys1.com/HitzFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.hitz_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.asempa_title),this.getResources().getString(R.string.asempa_tagline),"http://mmg.streamguys1.com/AsempaFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.asempa_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.adom_title),this.getResources().getString(R.string.adom_tagline),"http://mmg.streamguys1.com/AdomFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.adom_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.luv_title),this.getResources().getString(R.string.luv_tagline),"http://mmg.streamguys1.com/LuvFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.luv_fm,false));
        radioData.add(new RadioModel(this.getResources().getString(R.string.nhyira_title),this.getResources().getString(R.string.nhyira_tagline),"http://mmg.streamguys1.com/NhyiraFM-mp3?key=93fe20a16f78c70991fa726b9ca9c19c49f7329a3ad6144500e8fe7f3b8dadbafa6e84e66e84f9d149b17181fcf7194f",R.drawable.nhyira_fm,false));

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_ENDED:
                setPlayPause(!isPlaying);
                SharedExoPlayer.getInstance().exoPlayer.seekTo(0);
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;
            case Player.STATE_READY:
                showPlayButton(true);
                if (SharedExoPlayer.getInstance().exoPlayer!=null){
                    playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
                }
                if(playerState)
                {
                   setPlayPause(false);
                    isPlaying=true;
                }
                else
                {
                    setPlayPause(true);
                    isPlaying=false;
                }

                if ( SharedExoPlayer.getInstance().exoPlayer!=null) {
                    PlayerNotification.mStateBuilder.setState(playWhenReady ? PlaybackStateCompat.STATE_PLAYING : PlaybackStateCompat.STATE_PAUSED,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }
                break;
            case Player.STATE_BUFFERING:
                try {
                    showPlayButton(false);
                    PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_BUFFERING,
                            SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                }catch (Exception e ){

                }

                break;
            case Player.STATE_IDLE:
                PlayerNotification.mStateBuilder.setState(PlaybackStateCompat.STATE_NONE,
                        SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition(), 1f);
                break;

        }
        PlayerNotification.mMediaSession.setPlaybackState(PlayerNotification.mStateBuilder.build());
        PlayerNotification.showNotification(PlayerNotification.mStateBuilder.build(),songPositionclick,radioData);
    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }
    private void initFragment(Fragment homeFragment) {
        transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment, homeFragment);
        transaction.commit();
    }
    public void playExoPlayerRadio( int position,List<RadioModel> radioData){
        this.channelList=radioData;
        SharedExoPlayer.getInstance().PlayAudio(radioData.get(position).getSourceUrl(),radioData.get(position).getTitle(),"");
        this.songUrl=radioData.get(position).getSourceUrl();
        this.title=radioData.get(position).getTitle();
        songPosition=position;
        songPositionclick=position;
        playMusic();
    }
    private void playMusic() {

        if(InternetConnection.checkConnection(this)) {
            if (SharedExoPlayer.getInstance().exoPlayer != null) {
                this.startService(new Intent(this, NotiService.class));
                if (currentpos==SharedExoPlayer.getInstance().songPosition) {
                    if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_READY) {
                        showPlayButton(true);
                        boolean playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
                        if (playerState) {
                           setPlayPause(true);
                            isPlaying=true;
                        } else {
                           setPlayPause(false);
                            isPlaying=false;

                        }
                    } else if (SharedExoPlayer.getInstance().exoPlayer.getPlaybackState() == Player.STATE_BUFFERING) {
                        showPlayButton(false);
                    }
                }
                    else {
                        SharedExoPlayer.getInstance().PlayAudio(songUrl,title,"");

                    }
                } else {
                    SharedExoPlayer.getInstance().PlayAudio(songUrl,title,"");

                }
            } else {
                InternetConnection.AlertBox(this, "Please Check Your Internet Connection");

            }


    }




    public static class MediaReceiver extends BroadcastReceiver {

        public MediaReceiver() {
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            MediaButtonReceiver.handleIntent(PlayerNotification.mMediaSession, intent);


        }
    }


    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("shubham", "onReceive: "+intent.getAction());
          switch (intent.getAction()){
              case Constants.ACTION.PLAY_ACTION:
                  controlPlayer();
                  break;

              case Constants.ACTION.PRIVIOUS_ACTION:
                  priviousChannel();
                  break;

              case Constants.ACTION.FORWORD_ACTION:
                  forwordChannel();
                  break;
          }
        }
    };
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
            if (adView != null) {
                adView.setAdListener(null);
            }

    }



    @Override
    public void onBackPressed() {
       SharedExoPlayer.getInstance().endRadio();
        SharedExoPlayer.getInstance().addItemListner(null);
        notificationManager.cancelAll();
        super.onBackPressed();
    }

    private void setUpRecyclerView() {
        radioChannel.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        RecyclerView.LayoutManager lm = new GridLayoutManager(this, 2);
        radioChannel.setLayoutManager(lm);
        mAdapter = new RadioAdapter(this,radioData);
        radioChannel.setAdapter(mAdapter);
        radioChannel.addOnItemTouchListener(new RecyclerTouchListener(this, radioChannel, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position1) {
                recyclerPosition=position1;
                 manageModelData(position1, isback);
                showAd();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
    private void manageModelData(int position1, boolean isback) {
            if (isback){
                if (SharedExoPlayer.getInstance().exoPlayer==null) {
                    ((PlayerActivity) this).playExoPlayerRadio(position1, radioData);
                    setUpDataInView(position1);
                    saveStateInSession(position1);
                }else {
                    setUpDataInView(Application.shardPref.getInt(Constants.Pref.PLAYERSTATE,0));
                    setPlayPause(false);
                    saveStateInSession(Application.shardPref.getInt(Constants.Pref.PLAYERSTATE,0));
                }
                this.isback=false;
            }else{
                ((PlayerActivity) this).playExoPlayerRadio(position1, radioData);
                setUpDataInView(position1);
                saveStateInSession(position1);
            }
    }


    private void setUpDataInView(int position1) {
        recyclerPosition=position1;
        channelName.setText(radioData.get(position1).getTitle());
        channelName1.setText(radioData.get(position1).getTitle());
        tagLineAbove.setText(radioData.get(position1).getTagLine());
        if (radioModel!=null){
            radioModel.setIsPlay(false);
        }
        radioModel=radioData.get(position1);
        radioModel.setIsPlay(true);
        mAdapter.notifyDataSetChanged();

    }

    @OnClick(R.id.play)
    public void controlPlayer(){
        boolean playerState;
        playerState = SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady();
        if(playerState)
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }
        else
        {
            SharedExoPlayer.getInstance().checkAudio();
        }
        setPlayPause(playerState);
    }

    private void setPlayPause(boolean playerState) {
        isplayOrNot=playerState;
        if(!isplayOrNot){
            play.setImageResource(R.drawable.ic_pause);
        }else{
            play.setImageResource(R.drawable.ic_play);
        }


    }

    @OnClick(R.id.rewind)
    public void priviousChannel(){
        if (recyclerPosition<=0){
            return;
        }else{
            recyclerPosition--;
            if (recyclerPosition>=0 && recyclerPosition<radioData.size()){
                manageModelData(recyclerPosition,false);
            }
        }


    }

    @OnClick(R.id.forword)
    public void forwordChannel(){
        if (recyclerPosition>=radioData.size()){
            return;
        }else{
            recyclerPosition++;
            if (recyclerPosition<radioData.size()){
                manageModelData(recyclerPosition,false);
            }
        }

    }
    @OnClick(R.id.backPressed)
    public void backpressed(){
        onBackPressed();
    }

    @OnClick(R.id.volume)
    public void setMuteorUnmute(){
        audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (!isMute){
            volume.setImageResource(R.drawable.mute);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
            isMute=true;
        }else {
            volume.setImageResource(R.drawable.ic_volume);
            audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
            isMute=false;
        }
    }
    public void showPlayButton(boolean b) {
        if (b){
            play.setVisibility(View.VISIBLE);
            progressbar.setVisibility(View.INVISIBLE);
        }else {
            play.setVisibility(View.INVISIBLE);
            progressbar.setVisibility(View.VISIBLE);
        }
    }

    public void saveStateInSession(int position){
        session= Application.shardPref.edit();
        session.putInt(Constants.Pref.PLAYERSTATE,position );
        session.apply();
    }

}
