package com.multimedia.joyonline.view.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by rajesh on 9/1/17.
 */

@SuppressLint("AppCompatCustomView")
public class LatoMediumTextView extends TextView {
    public LatoMediumTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public LatoMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LatoMediumTextView(Context context) {
        super(context);
        init();
    }

    @SuppressLint("WrongConstant")
    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "Lato-Medium.ttf");
        setTypeface(tf ,Typeface.NORMAL);

    }

}
