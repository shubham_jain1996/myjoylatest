package com.multimedia.joyonline.model.model.podcast.podCastPlayer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Enclosure {
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("length")
    @Expose
    private Integer length;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

}
