package com.multimedia.joyonline.model.model.podcast.podCastPlayer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Guid {
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("permaLink")
    @Expose
    private String permaLink;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPermaLink() {
        return permaLink;
    }

    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }
}
