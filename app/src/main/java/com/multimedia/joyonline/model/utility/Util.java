package com.multimedia.joyonline.model.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * Created by root on 9/14/17.
 */

public class Util {


    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyymmddhhmmss", Locale.getDefault());


    public static File getCompressed(Context context, String path) throws IOException {

        if(context == null)
            throw new NullPointerException("Context must not be null.");

        File cacheDir = context.getExternalCacheDir();
        if(cacheDir == null)
            cacheDir = context.getCacheDir();

        String rootDir = cacheDir.getAbsolutePath() + "/ImageCompressor";
        File root = new File(rootDir);

        if(!root.exists())
            root.mkdirs();

        Bitmap bitmap = decodeImageFromFiles(path, /* your desired width*/400, /*your desired height*/ 400);

        //create placeholder for the compressed image file
        File compressed = new File(root, SDF.format(new Date()) + ".jpg" /*Your desired format*/);

        //convert the decoded bitmap to stream
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();


        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, byteArrayOutputStream);

        FileOutputStream fileOutputStream = new FileOutputStream(compressed);
        fileOutputStream.write(byteArrayOutputStream.toByteArray());
        fileOutputStream.flush();

        fileOutputStream.close();

        return compressed;
    }

    public static Bitmap decodeImageFromFiles(String path, int width, int height) {
        BitmapFactory.Options scaleOptions = new BitmapFactory.Options();
        scaleOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, scaleOptions);
        int scale = 1;
        while (scaleOptions.outWidth / scale / 2 >= width
                && scaleOptions.outHeight / scale / 2 >= height) {
            scale *= 2;
        }
        BitmapFactory.Options outOptions = new BitmapFactory.Options();
        outOptions.inSampleSize = scale;
        int rotate = 0;
        try {
            ExifInterface exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            Bitmap rotattedBitmap= BitmapFactory.decodeFile(path,outOptions);
            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);
            return Bitmap.createBitmap(rotattedBitmap, 0, 0, rotattedBitmap.getWidth(), rotattedBitmap.getHeight(), matrix, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return BitmapFactory.decodeFile(path, outOptions);
    }

    public static String parseDateToddMMyyyy(String time) {
        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat output = new SimpleDateFormat("MM/dd/yyyy");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }

    public static String splitDateTime(String time){
        StringTokenizer tokens = new StringTokenizer(time, "T");
        String first = tokens.nextToken();// this will contain "Fruit"
        String second = tokens.nextToken();
        String result=first;

        return result;
    }

    public static String parseDateToddMMyyyytext(String time) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd MMM-yyyy hh:mm a");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }

    public static String parseDate(String time) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }
    public static String parseDay(String time) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("EEEE");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }

    public static String parseTime(String time) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("hh:mm a");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }
    public static String parseDateWithName(String time) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd-MMM");
        Date oneWayTripDate = null;
        try {
            oneWayTripDate = input.parse(time);  // parse input
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output.format(oneWayTripDate);
    }

    public static AlertDialog.Builder AlertBox(Context context,String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setCancelable(true);
        builder1.setTitle("iFound");
        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();

        return builder1;
    }

    public static String parseDatefromServer(String givenDateString) {
        long dayagolong=0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {
            Date mDate = sdf.parse(givenDateString);
             dayagolong = mDate.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String dateWithFormat=parseDate(givenDateString);
        //API.log("Day Ago "+dayago);
        String result = "now";
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String todayDate = formatter.format(new Date());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(dayagolong);
        String agoformater = formatter.format(calendar.getTime());

        String day= parseDay(agoformater);
        String time1= parseTime(agoformater);
        String monthName=parseDateWithName(agoformater);
        Date CurrentDate = null;
        Date CreateDate = null;

        try {
            CurrentDate = formatter.parse(todayDate);
            CreateDate = formatter.parse(agoformater);

            long different = Math.abs(CurrentDate.getTime() - CreateDate.getTime());

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = different / daysInMilli;
            different = different % daysInMilli;

            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            different = different % secondsInMilli;
            if (elapsedDays == 0) {
                if (elapsedHours == 0) {
                    if (elapsedMinutes == 0) {
                        if (elapsedSeconds < 0) {
                            return "0" + " s";
                        } else {
                            if (elapsedDays > 0 && elapsedSeconds < 59) {
                                return "now";
                            }
                        }
                    } else {
                        return String.valueOf(elapsedMinutes) + " minutes ago";
                    }
                } else {
                    return String.valueOf(elapsedHours) + " hours ago";
                }

            } else {
                if (elapsedDays < 1) {
                    return String.valueOf(elapsedHours) + " hours ago";
                }
                if (elapsedDays >= 1 && elapsedDays <= 7) {
                    // return day+" at "+time1;
                    return dateWithFormat;
                }
                if (elapsedDays > 7 && elapsedDays <= 29) {
                    //   return monthName+" at "+time1;
                    return dateWithFormat;
                }
                if (elapsedDays > 29 && elapsedDays <= 58) {
                    //  return monthName+" at "+time1;
                    return dateWithFormat;
                }
                if (elapsedDays > 58 && elapsedDays <= 87) {
                    return dateWithFormat;
                }
                if (elapsedDays > 87 && elapsedDays <= 116) {
                    return dateWithFormat;
                }
                if (elapsedDays > 116 && elapsedDays <= 145) {
                    return dateWithFormat;
                }
                if (elapsedDays > 145 && elapsedDays <= 174) {
                    return dateWithFormat;
                }
                if (elapsedDays > 174 && elapsedDays <= 203) {
                    return dateWithFormat;
                }
                if (elapsedDays > 203 && elapsedDays <= 232) {
                    return dateWithFormat;
                }
                if (elapsedDays > 232 && elapsedDays <= 261) {
                    return dateWithFormat;
                }
                if (elapsedDays > 261 && elapsedDays <= 290) {
                    return dateWithFormat;
                }
                if (elapsedDays > 290 && elapsedDays <= 319) {
                    return dateWithFormat;
                }
                if (elapsedDays > 319 && elapsedDays <= 348) {
                    return dateWithFormat;
                }
                if (elapsedDays > 348 && elapsedDays <= 360) {
                    return dateWithFormat;
                }

                if (elapsedDays > 360 && elapsedDays <= 720) {
                    return "1 years ago";
                }

                if (elapsedDays > 720) {
                    SimpleDateFormat formatterYear = new SimpleDateFormat("MM/dd/yyyy");
                    Calendar calendarYear = Calendar.getInstance();
                    calendarYear.setTimeInMillis(dayagolong);
                    return formatterYear.format(calendarYear.getTime()) + "";
                }

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }
    public static String stringForTime(int timeMs) {
        StringBuilder mFormatBuilder;
        Formatter mFormatter;
        mFormatBuilder = new StringBuilder();
        mFormatter = new Formatter(mFormatBuilder, Locale.getDefault());
        int totalSeconds = timeMs / 1000;
        //int totalSeconds =  timeMs ;

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        mFormatBuilder.setLength(0);
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

}
