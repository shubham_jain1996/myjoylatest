package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.model.podcast.CollectionList;

import java.util.ArrayList;
import java.util.List;


public class PodcastChannelAdapter extends RecyclerView.Adapter<PodcastChannelAdapter.ViewHolder> {
    private Context context;
    List<CollectionList> data= new ArrayList<>();


    public PodcastChannelAdapter(Context context, List<CollectionList> data) {
        this.context = context;

        this.data=data;

    }


    @Override
    public PodcastChannelAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.podcast_channel_adapter, viewGroup, false);
        return new PodcastChannelAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PodcastChannelAdapter.ViewHolder viewHolder, int position) {
      //  viewHolder.fmImage.setImageResource(data.get(position).getIcon());
        if (data.get(position).getId()==1){
            viewHolder.fmImage.setImageResource(R.drawable.adom_fm);
        }else if (data.get(position).getId()==5){
            viewHolder.fmImage.setImageResource(R.drawable.asempa_fm);
        }else if (data.get(position).getId()==6){
            viewHolder.fmImage.setImageResource(R.drawable.nhyira_fm);
        }else if (data.get(position).getId()==7){
            viewHolder.fmImage.setImageResource(R.drawable.ic_joyfm);
        }
        viewHolder.fmName.setText(data.get(position).getName());
        viewHolder.tagLine.setText(data.get(position).getDescription());
    /*    if (data.get(position).getIsPlay()){
            viewHolder.play.setVisibility(View.VISIBLE);
        }else {
            viewHolder.play.setVisibility(View.INVISIBLE);
        }*/

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView fmImage;
        TextView fmName, tagLine,play;
         ViewHolder(View view) {
            super(view);
            fmImage=view.findViewById(R.id.fm_image);
             fmName=view.findViewById(R.id.channelName);
             tagLine=view.findViewById(R.id.tagLine);
             play=view.findViewById(R.id.play);


        }
    }
}