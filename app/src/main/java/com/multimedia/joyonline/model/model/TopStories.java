package com.multimedia.joyonline.model.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.joyonline.model.model.homeModel.List_;

public class TopStories {
    @SerializedName("list")
    @Expose
    private java.util.List<List_> list = null;
    @SerializedName("more_stories")
    @Expose
    private String moreStories;

    public java.util.List<List_> getList() {
        return list;
    }

    public void setList(java.util.List<List_> list) {
        this.list = list;
    }

    public String getMoreStories() {
        return moreStories;
    }

    public void setMoreStories(String moreStories) {
        this.moreStories = moreStories;
    }
}
