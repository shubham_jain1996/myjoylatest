package com.multimedia.joyonline.model.model.homeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.joyonline.model.model.homeModel.List;

public class LocalNews {
    @SerializedName("list")
    @Expose
    private java.util.List<List> list = null;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }
}
