package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.podcast.ShowItem;
import com.multimedia.joyonline.model.model.podcast.podCastPlayer.PodastModel;
import com.multimedia.joyonline.model.utility.Util;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.gresse.hugo.vumeterlibrary.VuMeterView;


public class PodCast_Player_Adapter extends RecyclerView.Adapter<PodCast_Player_Adapter.ViewHolder> {
    private Context context;

    List<PodastModel> PlayerEpisodeList= new ArrayList<>();


    public PodCast_Player_Adapter(Context context,  List<PodastModel> PlayerEpisodeList) {
        this.context = context;
        this.PlayerEpisodeList=PlayerEpisodeList;

    }


    @Override
    public PodCast_Player_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.podcast_player_adapter, viewGroup, false);
        return new PodCast_Player_Adapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PodCast_Player_Adapter.ViewHolder viewHolder, int position) {
        if (PlayerEpisodeList.get(position).getIsplay()){
           // viewHolder.play.setImageResource(R.drawable.pause_grey);
         //   if (PlayerEpisodeList.get(position).getPause()){
            /*    viewHolder.vumeter.setVisibility(View.GONE);
                viewHolder.play.setVisibility(View.VISIBLE);*/
                viewHolder.play.setImageResource(R.drawable.pause_grey);
           /* }else {
                viewHolder.play.setVisibility(View.GONE);
                viewHolder.vumeter.setVisibility(View.VISIBLE);
                viewHolder.vumeter.setColor(context.getResources().getColor(R.color.red));
            }*/

        }else {
          //  viewHolder.play.setVisibility(View.VISIBLE);
            viewHolder.play.setImageResource(R.drawable.play_grey);
          //  viewHolder.vumeter.setVisibility(View.GONE);
        }

        viewHolder.episodeName.setText(PlayerEpisodeList.get(position).getTitle());
        viewHolder.channelName.setText(PlayerEpisodeList.get(position).getDescription());
        viewHolder.date.setText(Util.parseDateToddMMyyyytext(PlayerEpisodeList.get(position).getPubDate()));


    }
    @Override
    public int getItemCount() {
            return PlayerEpisodeList.size();

    }

     static class ViewHolder extends RecyclerView.ViewHolder {
       ImageButton play;
        TextView episodeName, date,channelName;
         VuMeterView vumeter;
         ViewHolder(View view) {
            super(view);

             vumeter=view.findViewById(R.id.vumeter);
             play=view.findViewById(R.id.play);
             episodeName=view.findViewById(R.id.episodeName);
             date=view.findViewById(R.id.date);
             channelName=view.findViewById(R.id.channelName);



        }
    }

}