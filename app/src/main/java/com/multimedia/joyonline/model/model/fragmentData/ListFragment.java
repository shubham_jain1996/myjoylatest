package com.multimedia.joyonline.model.model.fragmentData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListFragment  implements Parcelable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("summary")
    @Expose
    private String summary;
    @SerializedName("timestamp")
    @Expose
    private String timestamp;
    @SerializedName("bookmarked")
    @Expose
    private String bookmarked;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("detail_url")
    @Expose
    private String detailUrl;
    public final static Parcelable.Creator<ListFragment> CREATOR = new Creator<ListFragment>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ListFragment createFromParcel(Parcel in) {
            return new ListFragment(in);
        }

        public ListFragment[] newArray(int size) {
            return (new ListFragment[size]);
        }

    }
            ;

    protected ListFragment(Parcel in) {
        this.id = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.summary = ((String) in.readValue((String.class.getClassLoader())));
        this.timestamp = ((String) in.readValue((String.class.getClassLoader())));
        this.bookmarked = ((String) in.readValue((String.class.getClassLoader())));
        this.imageUrl = ((String) in.readValue((String.class.getClassLoader())));
        this.detailUrl = ((String) in.readValue((String.class.getClassLoader())));
    }

    public ListFragment() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(String bookmarked) {
        this.bookmarked = bookmarked;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDetailUrl() {
        return detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(summary);
        dest.writeValue(timestamp);
        dest.writeValue(bookmarked);
        dest.writeValue(imageUrl);
        dest.writeValue(detailUrl);
    }

    public int describeContents() {
        return 0;
    }
}
