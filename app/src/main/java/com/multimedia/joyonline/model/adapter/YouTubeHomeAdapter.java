package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.youtubeFiles.YoutubeVedioList;
import com.multimedia.joyonline.model.utility.Util;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.YouTubeVideos;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class YouTubeHomeAdapter extends RecyclerView.Adapter<YouTubeHomeAdapter.ViewHolder> {
    private Context context;
    YoutubeVedioList data;


    public YouTubeHomeAdapter(Context context, YoutubeVedioList data) {
        this.context = context;
        this.data=data;

    }


    @Override
    public YouTubeHomeAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.youtube_home_adapter, viewGroup, false);
        return new YouTubeHomeAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(YouTubeHomeAdapter.ViewHolder viewHolder, int position) {
        Picasso.get().load("https://i.ytimg.com/vi/"+data.getItems().get(position).getSnippet().getResourceId().getVideoId()+"/sddefault.jpg").into(viewHolder.vedioImage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progress_view.setVisibility(View.GONE);
            }

            @Override
            public void onError(Exception e) {
                Log.d("abc", "onError: "+e);
                viewHolder.progress_view.setVisibility(View.GONE);
                Picasso.get().load("https://i.ytimg.com/vi/"+data.getItems().get(position).getSnippet().getResourceId().getVideoId()+"/mqdefault.jpg").into(viewHolder.vedioImage);

                }
        });
        viewHolder.title.setText(data.getItems().get(position).getSnippet().getTitle());
        viewHolder.vedioTime.setText(Util.parseDatefromServer(data.getItems().get(position).getSnippet().getPublishedAt()));
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.adCounter+=1;
                Intent i = new Intent(context, YouTubeVideos.class);
                i.putExtra("listData",  data);
                i.putExtra("playlistId",data.getItems().get(position).getSnippet().getPlaylistId());
                i.putExtra("position",position);
                i.putExtra("type","inter");
                context.startActivity(i);
            }
        });

    }
    @Override
    public int getItemCount() {
        if (data!=null){
            return data.getItems().size();
        }else{
            return 0;
        }

    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView vedioImage;
        TextView title,vedioTime;
        CircularProgressView progress_view;
         ViewHolder(View view) {
            super(view);
             progress_view=view.findViewById(R.id.progress_view);
             vedioImage=view.findViewById(R.id.vedioImage);
             title=view.findViewById(R.id.title);
             vedioTime=view.findViewById(R.id.vedioTime);



        }
    }
}