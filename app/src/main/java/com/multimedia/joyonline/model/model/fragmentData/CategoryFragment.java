package com.multimedia.joyonline.model.model.fragmentData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryFragment implements Parcelable
{

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("more_stories")
    @Expose
    private String moreStories;
    @SerializedName("list")
    @Expose
    private java.util.List<ListFragment> list = null;
    public final static Parcelable.Creator<CategoryFragment> CREATOR = new Creator<CategoryFragment>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CategoryFragment createFromParcel(Parcel in) {
            return new CategoryFragment(in);
        }

        public CategoryFragment[] newArray(int size) {
            return (new CategoryFragment[size]);
        }

    }
            ;

    protected CategoryFragment(Parcel in) {
        this.categoryId = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.categoryName = ((String) in.readValue((String.class.getClassLoader())));
        this.moreStories = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.list, (ListFragment.class.getClassLoader()));
    }

    public CategoryFragment() {
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getMoreStories() {
        return moreStories;
    }

    public void setMoreStories(String moreStories) {
        this.moreStories = moreStories;
    }

    public java.util.List<ListFragment> getList() {
        return list;
    }

    public void setList(java.util.List<ListFragment> list) {
        this.list = list;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(categoryId);
        dest.writeValue(categoryName);
        dest.writeValue(moreStories);
        dest.writeList(list);
    }

    public int describeContents() {
        return 0;
    }
}
