package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.model.podcast.ShowItem;
import com.multimedia.joyonline.model.model.podcast.ShowsModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class Popular_Podcast_Adapter extends RecyclerView.Adapter<Popular_Podcast_Adapter.ViewHolder> {
    private Context context;

    List<ShowItem> showItemData= new ArrayList<>();


    public Popular_Podcast_Adapter(Context context,    List<ShowItem> showItemData) {
        this.context = context;
        this.showItemData=showItemData;

    }


    @Override
    public Popular_Podcast_Adapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.podcast_adapter, viewGroup, false);
        return new Popular_Podcast_Adapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(Popular_Podcast_Adapter.ViewHolder viewHolder, int position) {
        Log.d("suman", "data: "+showItemData.get(position).getImage());
      //  viewHolder.podcastImage.setImageResource(showsData.getItems().get(position).get());
            Glide.with(context)
                .load(showItemData.get(position).getImage())
                .centerCrop().listener(new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                    viewHolder.progress_view.setVisibility(View.INVISIBLE);
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    viewHolder.progress_view.setVisibility(View.INVISIBLE);
                    return false;
                }
            })

                .into(viewHolder.podcastImage);
      /*  Picasso.get().load(showItemData.get(position).getImage()).error(R.drawable.myjoylogo).networkPolicy(NetworkPolicy.OFFLINE).into(viewHolder.podcastImage, new Callback() {
            @Override
            public void onSuccess() {
                viewHolder.progress_view.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError(Exception e) {
                Log.d("suman", "onError: "+showItemData.get(position).getImage());
                viewHolder.progress_view.setVisibility(View.INVISIBLE);
            }
        });*/
        viewHolder.titleName.setText(showItemData.get(position).getName());
        viewHolder.channelName.setText(showItemData.get(position).getDescription());


    }
    @Override
    public int getItemCount() {
            return showItemData.size();

    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView podcastImage;
        TextView titleName, channelName;
        CircularProgressView progress_view;
         ViewHolder(View view) {
            super(view);
             progress_view=view.findViewById(R.id.progress_view);
             podcastImage=view.findViewById(R.id.podcastImage);
             titleName=view.findViewById(R.id.titleName);
             channelName=view.findViewById(R.id.channelName);



        }
    }
}