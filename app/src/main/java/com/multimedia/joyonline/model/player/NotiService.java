package com.multimedia.joyonline.model.player;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.session.MediaSession;
import android.os.Handler;
import android.os.IBinder;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.Timeline;
import com.multimedia.joyonline.model.utility.Constants;

public class NotiService extends Service implements SharedExoPlayer.SharedExoPlayerListner {

    private Handler handler;
    NotificationManager mNotificationManager;
    private MediaSession mSession;

    public NotiService() {
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_STICKY;
    }
   @Override
    public void onTaskRemoved(Intent rootIntent){
       removeNoti();

        super.onTaskRemoved(rootIntent);
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public  void removeNoti()
    {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(105);

    }


    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {

    }
}
