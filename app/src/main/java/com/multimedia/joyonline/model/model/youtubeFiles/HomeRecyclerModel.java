package com.multimedia.joyonline.model.model.youtubeFiles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HomeRecyclerModel {
    @SerializedName("category_Name")
    @Expose
    private String category_Name;
    @SerializedName("category_Id")
    @Expose
    private String category_Id;

    @SerializedName("data")
    @Expose
    private YoutubeVedioList topStories = null;

    public HomeRecyclerModel() {
    }

    public HomeRecyclerModel(String category_Name, String category_Id, YoutubeVedioList topStories) {
        this.category_Name = category_Name;
        this.category_Id = category_Id;
        this.topStories = topStories;
    }

    public String getCategory_Name() {
        return category_Name;
    }

    public void setCategory_Name(String category_Name) {
        this.category_Name = category_Name;
    }

    public String getCategory_Id() {
        return category_Id;
    }

    public void setCategory_Id(String category_Id) {
        this.category_Id = category_Id;
    }

    public YoutubeVedioList getHomedata() {
        return topStories;
    }

    public void setHomeData(YoutubeVedioList topStories) {
        this.topStories = topStories;
    }
}
