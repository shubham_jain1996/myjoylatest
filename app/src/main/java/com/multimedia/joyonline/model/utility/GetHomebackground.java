package com.multimedia.joyonline.model.utility;

import android.os.AsyncTask;
import android.text.Html;

import com.multimedia.joyonline.model.model.youtubeFiles.HomeRecyclerModel;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;
import com.multimedia.joyonline.model.model.youtubeFiles.YoutubeVedioList;
import com.multimedia.joyonline.model.network.DataService;

import java.util.ArrayList;
import java.util.List;

public class GetHomebackground extends AsyncTask<PlayListModel, String, HomeRecyclerModel> {
  private DataLoadingInterfaceHome callBack;
    HomeRecyclerModel homeData ;
  public GetHomebackground(DataLoadingInterfaceHome dataLoading){
      this.callBack = dataLoading;
  }
  @Override
  protected void onPostExecute(HomeRecyclerModel homeClasses) {
      callBack.postExecute(homeClasses);
  }

  @Override
  protected void onPreExecute() {
      callBack.preExecute();
  }

  @Override
  protected HomeRecyclerModel doInBackground(PlayListModel... params) {
      PlayListModel homeRecyclerModel = params[0];
      getYouTubeVedios(homeRecyclerModel.getTitle(),homeRecyclerModel.getPlayListId());
     /* for (int i=0; i<homeRecyclerModel.getHomedata().size(); i++){

      }*/
      return homeData;
  }
    private void getYouTubeVedios(String title, String playListId) {
        DataService dataService = new DataService();
        dataService.setOnServerListener(new DataService.ServerResponseListner<YoutubeVedioList>() {
            @Override
            public void onDataSuccess(YoutubeVedioList response) {
                if (response!=null){
                    homeData=new HomeRecyclerModel(title,playListId,response);

                  /*  mainAdapter.notifyDataSetChanged();*/

                  /*  youtubeVedioLists=response;
                    mAdapter = new YouTubeVedioAdapter(getActivity(),youtubeVedioLists);
                    playListsView.setAdapter(mAdapter);
                    if (response.getNextPageToken()!=null) {
                        token = response.getNextPageToken();
                    }*/
                    // mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onDataFailiure() {

            }
        });
        dataService.getYouTubeVedios4(playListId);
    }

}
