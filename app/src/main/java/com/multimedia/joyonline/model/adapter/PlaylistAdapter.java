package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ViewHolder> {
    private Context context;
    List<PlayListModel> data= new ArrayList<>();


    public PlaylistAdapter(Context context, List<PlayListModel> data) {
        this.context = context;

        this.data=data;

    }


    @Override
    public PlaylistAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.radio_adapter, viewGroup, false);
        return new PlaylistAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(PlaylistAdapter.ViewHolder viewHolder, int position) {
        Picasso.get().load(data.get(position).getImageUrl()).into( viewHolder.fmImage);
        viewHolder.fmName.setText(data.get(position).getTitle());
        viewHolder.tagLine.setVisibility(View.GONE);
        viewHolder.play.setVisibility(View.GONE);

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView fmImage;
        TextView fmName, tagLine,play;
         ViewHolder(View view) {
            super(view);
            fmImage=view.findViewById(R.id.fm_image);
             fmName=view.findViewById(R.id.channelName);
             tagLine=view.findViewById(R.id.tagLine);
             play=view.findViewById(R.id.play);


        }
    }
}