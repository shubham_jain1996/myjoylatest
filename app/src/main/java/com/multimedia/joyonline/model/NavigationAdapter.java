package com.multimedia.joyonline.model;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.homeModel.SideMenuList;


import java.util.ArrayList;
import java.util.List;


public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.ViewHolder> {
    private Context context;
    private List<SideMenuList> mFragmentTagList = new ArrayList<>();





   /* public NavigationAdapter(Context context, ArrayList<Integer> imagesArray, ArrayList<Integer> TITLE) {
        this.IMAGES=imagesArray;
        this.TITLE=TITLE;
        this.context=context;
    }*/

    public NavigationAdapter(Context context, List<SideMenuList> mFragmentTagList) {
        this.mFragmentTagList=mFragmentTagList;
        this.context=context;
    }


    @Override
    public NavigationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.navigationitem, viewGroup, false);
        return new NavigationAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(NavigationAdapter.ViewHolder viewHolder, int position) {
        viewHolder.itemName.setText(mFragmentTagList.get(position).getTitle());
       // viewHolder.itemName.setCompoundDrawablesWithIntrinsicBounds(Icon.get(position), 0, 0, 0);

        // viewHolder.itemImage.setImageResource(IMAGES.get(position));


    }
    @Override
    public int getItemCount() {
        return mFragmentTagList.size();
    }

     static class ViewHolder extends RecyclerView.ViewHolder {
       TextView itemName;
       ImageView itemImage;
         ViewHolder(View view) {
            super(view);
            itemName=view.findViewById(R.id.itemName);
         //   itemImage=view.findViewById(R.id.image);


        }
    }
}