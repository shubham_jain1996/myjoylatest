package com.multimedia.joyonline.model.network;


import com.multimedia.joyonline.model.model.fragmentData.MainModelFragment;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.podcast.CollectionList;
import com.multimedia.joyonline.model.model.podcast.ShowsModel;
import com.multimedia.joyonline.model.model.podcast.TokenClass;
import com.multimedia.joyonline.model.model.podcast.podCastPlayer.PodCast_MainModel;
import com.multimedia.joyonline.model.model.tv.YoutubeChannelList;
import com.multimedia.joyonline.model.model.youtubeFiles.YoutubeVedioList;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    Call<YoutubeVedioList> getYouTubeVedios(@Query("playlistId") String id, @Query("key") String key
            , @Query("part") String part, @Query("maxResults") String maxResults, @Query("pageToken") String pageToken);

    @GET("https://www.googleapis.com/youtube/v3/playlistItems")
    Call<YoutubeVedioList> getYouTubeVedios4(@Query("playlistId") String id, @Query("key") String key
            , @Query("part") String part, @Query("maxResults") String maxResults);
    @POST("homepage")
    Call<ServerResponse> getHomeData();

    @POST("categories")
    Call<ServerResponse> getNewsData();

    @GET("https://www.googleapis.com/youtube/v3/search")
    Call<YoutubeChannelList> getAdomTvResult(@Query("channelId") String id, @Query("key") String key
            , @Query("part") String part, @Query("eventType") String maxResults, @Query("type") String pageToken);


    @FormUrlEncoded
   // @POST("/oauth/token")
    @POST("https://atunwadigital-recast.streamguys1.com/oauth/token")
    Call<TokenClass> generateToken(@Field("username") String username, @Field("password") String password, @Field("client_secret") String client_sceret, @Field("client_id") String client_id, @Field("scope") String scope, @Field("grant_type") String grant_type);


    @GET("https://atunwadigital-recast.streamguys1.com/api/v1/collections")
    Call<List<CollectionList>> getCollectionData(@Header("Authorization") String token);

    @GET("https://atunwadigital-recast.streamguys1.com/api/v1/collections/view/{id}")
    Call<ShowsModel> getShowsList(@Header("Authorization") String token,@Path("id") String id);


    @GET("http://atunwadigital-recast.streamguys1.com/api/v1/sgrecast/podcasts/feeds/episodes/{id}")
    Call<PodCast_MainModel> getPodCastPlayerList(@Header("Authorization") String token, @Path("id") String id, @Query("page") String part);
}
