package com.multimedia.joyonline.model.utility;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.concurrent.TimeUnit;

public class InternetConnection {
    public static boolean checkConnection(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connMgr == null) {
            return false;
        }
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        return activeNetworkInfo != null && (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI || activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    public static void AlertBox(Context context,String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(message);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    public static boolean checkValidity(long buy_ms)
    {
        if(buy_ms !=0L)
        {
            long buy_minutes = TimeUnit.MILLISECONDS.toMinutes(buy_ms);
            long current_ms = System.currentTimeMillis();
            long current_min = (current_ms / 1000) / 60;
            long day90_milli = 60 * 24 * 365;
            long end_min = buy_minutes + day90_milli;
            long seconds = TimeUnit.MILLISECONDS.toSeconds(buy_ms);
            if (end_min > current_min)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else {
            return false;
        }
    }
}
