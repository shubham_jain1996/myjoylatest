package com.multimedia.joyonline.model.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.widget.Toast;


import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.fragmentData.MainModelFragment;
import com.multimedia.joyonline.model.model.homeModel.ServerResponse;
import com.multimedia.joyonline.model.model.podcast.CollectionList;
import com.multimedia.joyonline.model.model.podcast.ShowsModel;
import com.multimedia.joyonline.model.model.podcast.TokenClass;
import com.multimedia.joyonline.model.model.podcast.podCastPlayer.PodCast_MainModel;
import com.multimedia.joyonline.model.model.tv.YoutubeChannelList;
import com.multimedia.joyonline.model.model.youtubeFiles.YoutubeVedioList;
import com.multimedia.joyonline.model.utility.InternetConnection;
import com.multimedia.joyonline.view.dialoge.ProgressDialogue;
import com.multimedia.joyonline.view.mainActivity.fragment.registerFragment.YouTubePlaylists;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataService {
    private String message = "Loading";
    Context context;


    public interface ServerResponseListner<T> {
        public void onDataSuccess(T response);
        public void onDataFailiure();
    }
    public DataService(Context context)
    {
        helpDialog= new ProgressDialogue(context);
        helpDialog.setCancelable(false);
        this.context = context;
    }

    public DataService(){

    }

    private ServerResponseListner serverResponseListner;
    public void setOnServerListener(ServerResponseListner listener) {
        serverResponseListner = listener;
    }

 ProgressDialogue helpDialog;
    private ProgressDialog progressDialogue;

    private void showLoader(String message){

        helpDialog.show();
    }

    public void getHomeData() {
    /*    if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }*/
       // showLoader(message);
      Call<ServerResponse> call = RetrofitInstance.getApiInstance().getHomeData();
        performServerRequest(call);

    }


    public void getNewsData() {
    /*    if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }*/
        // showLoader(message);
        Call<ServerResponse> call = RetrofitInstance.getApiInstance().getNewsData();
        performServerRequest(call);

    }


    public void getYouTubeVedios(String playListId,String token) {
       if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<YoutubeVedioList> call = RetrofitInstance.getRetrofitInstance().getYouTubeVedios(playListId,"AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
        "20",token);
        performServerRequest(call);

    }
    public void getYouTubeVedios4(String playListId) {
        /*if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }*/
       // showLoader(message);
        Call<YoutubeVedioList> call = RetrofitInstance.getRetrofitInstance().getYouTubeVedios4(playListId,"AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "4");
        performServerRequest(call);

    }

    public void getMyJoyChannelId() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }

        Call<YoutubeChannelList> call = RetrofitInstance.getApiInstance().getAdomTvResult("UChd1DEecCRlxaa0-hvPACCw","AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU","snippet",
                "live","video");
        performServerRequest(call);

    }



    public void generateToken() {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<TokenClass> call = RetrofitInstance.getPodcastInstance().generateToken("atunwadigital@streamguys.com","GVr9PepskKT8EVee","siyKM1gsHZzaNVbYR8ofQAplDRzIdXq3ipsMJK3Y",
                "2","*","password");
        performServerRequest(call);

    }


    public void getCollectionData(String token) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<List<CollectionList>> call = RetrofitInstance.getPodcastInstance().getCollectionData(token);
        performServerRequest(call);

    }

    public void getShowsList(String token, String channelId) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<ShowsModel> call = RetrofitInstance.getPodcastInstance().getShowsList(token,channelId);
        performServerRequest(call);

    }
    public void  getPodCastPlayerList(String token, String pageNo,String channelId) {
        if (!InternetConnection.checkConnection(context)) {
            Toast.makeText(context, R.string.no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }
        showLoader(message);
        Call<PodCast_MainModel> call = RetrofitInstance.getPodcastInstance().getPodCastPlayerList(token,channelId,pageNo);
        performServerRequest(call);

    }























    private void performServerRequest(Call call)
    {
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call call, Response response) {
              //  kProgressHUD.dismiss();
                if (helpDialog != null) {
                  helpDialog.dismiss();
                }

                if (serverResponseListner != null) {
                    serverResponseListner.onDataSuccess(response.body());

                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
             //   kProgressHUD.dismiss();
                if (helpDialog != null) {
                   helpDialog.dismiss();
                }

                if (serverResponseListner != null) {
                    serverResponseListner.onDataFailiure();
                }
                if (context != null) {
                    if (t instanceof IOException) {
                        Toast.makeText(context, R.string.no_internet_connection+t.getMessage(), Toast.LENGTH_SHORT).show();
                        // logging probably not necessary
                    }
                    else {
                        Toast.makeText(context, "Parsing Error", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        });
    }



}

