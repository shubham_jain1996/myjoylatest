package com.multimedia.joyonline.model.model.podcast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Syndications {
    @SerializedName("iTunes")
    @Expose
    private String iTunes;
    @SerializedName("googlePlay")
    @Expose
    private String googlePlay;
    @SerializedName("tuneIn")
    @Expose
    private String tuneIn;

    public String getITunes() {
        return iTunes;
    }

    public void setITunes(String iTunes) {
        this.iTunes = iTunes;
    }

    public String getGooglePlay() {
        return googlePlay;
    }

    public void setGooglePlay(String googlePlay) {
        this.googlePlay = googlePlay;
    }

    public String getTuneIn() {
        return tuneIn;
    }

    public void setTuneIn(String tuneIn) {
        this.tuneIn = tuneIn;
    }
}
