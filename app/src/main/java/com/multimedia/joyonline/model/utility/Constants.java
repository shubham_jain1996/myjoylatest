package com.multimedia.joyonline.model.utility;



public class Constants {


    public static final Boolean STATUS =true ;
    public static final String PREF_NAME ="subcon" ;

    public interface ACTION {
        public static String PLAY_ACTION = "play_joy";
        public static String PAUSE_ACTION = "pause";
        public static String PRIVIOUS_ACTION = "backword_joy";
        public static String FORWORD_ACTION = "forword_joy";

    }

    public class Pref {
        public static final String PLAYERSTATE ="playerState" ;
        public static final String TOKEN ="token" ;
    }
}
