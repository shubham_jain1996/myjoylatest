package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.fragmentData.DataFragment;

public class fragmentAdapter extends RecyclerView.Adapter {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_ITEM1 = 1;
        private final int VIEW_TYPE_ITEM2 = 2;
        private final int VIEW_TYPE_ITEM3 = 3;

    Context context;
    private SportsNewsAdapter sportsNewsAdapter;
    DataFragment dataFragment;

    public fragmentAdapter(Context context, DataFragment moviesList) {
        this.dataFragment = moviesList;
        this.context=context;

    }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType==VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder1(view);
            }else   if (viewType == VIEW_TYPE_ITEM1) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder1(view);
            }else if (viewType == VIEW_TYPE_ITEM2){
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder2(view);
            }else{
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.singledata_fragment, parent, false);
                return new ItemViewHolder3(view);
            }

        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder instanceof ItemViewHolder) {
                populateItemRows((ItemViewHolder) viewHolder, position);
            } else if (viewHolder instanceof ItemViewHolder3) {
                populateItemRows3((ItemViewHolder3) viewHolder, position);
            }else if (viewHolder instanceof ItemViewHolder1){
                populateItemRows1((ItemViewHolder1) viewHolder, position);
            }
            else if (viewHolder instanceof ItemViewHolder2){
                populateItemRows2((ItemViewHolder2) viewHolder, position);
            }

        }

        @Override
        public int getItemCount() {
        if (dataFragment==null){
            return 0;
        }else{
            return dataFragment.getCategories().size();
        }

        }

        /**
         * The following method decides the type of ViewHolder to display in the RecyclerView
         *
         * @param position
         * @return
         */
        @Override
        public int getItemViewType(int position) {
            if (position==0){
                return VIEW_TYPE_ITEM;
            }else if (position==1){
                return VIEW_TYPE_ITEM1;
            }else if (position==2){
                return VIEW_TYPE_ITEM2 ;
            }else{
                return VIEW_TYPE_ITEM3;
            }

        }


        private class ItemViewHolder extends RecyclerView.ViewHolder {
           RecyclerView data;
            LinearLayoutManager layoutManager;
            ViewPager mPager;
            public ItemViewHolder(@NonNull View itemView) {
                super(itemView);
                mPager =  itemView.findViewById(R.id.newsPager);
                data=itemView.findViewById(R.id.data);
                data.setHasFixedSize(true);
                layoutManager=new LinearLayoutManager(context);
                RecyclerView.LayoutManager horizontalLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
                data.setLayoutManager(horizontalLayoutManager);
            }
        }
    private class ItemViewHolder1 extends RecyclerView.ViewHolder {
        RecyclerView data;
        LinearLayoutManager layoutManager;


        public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            data=itemView.findViewById(R.id.data);
            data.setHasFixedSize(true);
            layoutManager=new LinearLayoutManager(context);
            RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 1);
            data.setLayoutManager(verticalLayoutManager);

        }
    }

    private class ItemViewHolder3 extends RecyclerView.ViewHolder {
        RecyclerView data;
        LinearLayoutManager layoutManager;
        public ItemViewHolder3(@NonNull View itemView) {
            super(itemView);
            data=itemView.findViewById(R.id.data);
            data.setHasFixedSize(true);
            layoutManager=new LinearLayoutManager(context);
            RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 2);
            data.setLayoutManager(verticalLayoutManager);
        }
    }
        private class ItemViewHolder2 extends RecyclerView.ViewHolder {

            RecyclerView data;
            LinearLayoutManager layoutManager;

            public ItemViewHolder2(@NonNull View itemView) {
                super(itemView);
                data=itemView.findViewById(R.id.data);
                data.setHasFixedSize(true);
                layoutManager=new LinearLayoutManager(context);
                RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 2);
                data.setLayoutManager(verticalLayoutManager);
            }
        }

        private void populateItemRows2(ItemViewHolder2 viewHolder, int position) {
            if (dataFragment.getCategories().size()>0) {
                sportsNewsAdapter = new SportsNewsAdapter(context, dataFragment.getCategories().get(0).getList());
                viewHolder.data.setAdapter(sportsNewsAdapter);
            }
        }

        private void populateItemRows(ItemViewHolder viewHolder, int i) {


        }
    private void populateItemRows1(ItemViewHolder1 viewHolder, int i) {

    }
    private void populateItemRows3(ItemViewHolder3 viewHolder, int i) {

    }
    }

