package com.multimedia.joyonline.model.model.fragmentData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataFragment implements Parcelable
{

    @SerializedName("categories")
    @Expose
    private List<CategoryFragment> categories = null;
    public final static Parcelable.Creator<DataFragment> CREATOR = new Creator<DataFragment>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DataFragment createFromParcel(Parcel in) {
            return new DataFragment(in);
        }

        public DataFragment[] newArray(int size) {
            return (new DataFragment[size]);
        }

    }
            ;

    protected DataFragment(Parcel in) {
        in.readList(this.categories, (CategoryFragment.class.getClassLoader()));
    }

    public DataFragment() {
    }

    public List<CategoryFragment> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryFragment> categories) {
        this.categories = categories;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(categories);
    }

    public int describeContents() {
        return 0;
    }
}
