package com.multimedia.joyonline.model.model.homeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.multimedia.joyonline.model.model.TopStories;

import java.util.List;

public class Data {
    @SerializedName("categories")
    @Expose
    private List<Category> categories = null;
    @SerializedName("latest_news")
    @Expose
    private List<LatestNews> latestNews = null;
    @SerializedName("local_news")
    @Expose
    private LocalNews localNews;
    @SerializedName("top_stories")
    @Expose
    private TopStories topStories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<LatestNews> getLatestNews() {
        return latestNews;
    }

    public void setLatestNews(List<LatestNews> latestNews) {
        this.latestNews = latestNews;
    }

    public LocalNews getLocalNews() {
        return localNews;
    }

    public void setLocalNews(LocalNews localNews) {
        this.localNews = localNews;
    }

    public TopStories getTopStories() {
        return topStories;
    }

    public void setTopStories(TopStories topStories) {
        this.topStories = topStories;
    }

}
