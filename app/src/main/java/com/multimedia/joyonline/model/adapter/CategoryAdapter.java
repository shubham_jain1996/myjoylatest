package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.homeModel.Data;

public class CategoryAdapter extends RecyclerView.Adapter {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_ITEM1 = 1;


    Context context;
    private SportsNewsAdapter sportsNewsAdapter;
    Data dataFragment;
    private CategoryItem1Adapter categoryitem1Adapter;
    private CategoryItemAdapter categoryitemAdapter;

    public CategoryAdapter(Context context, Data moviesList) {
        this.dataFragment = moviesList;
        this.context=context;

    }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType==VIEW_TYPE_ITEM) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_fragment, parent, false);
                return new ItemViewHolder1(view);
            }else{
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_item_fragment, parent, false);
                return new ItemViewHolder(view);
            }

        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder instanceof ItemViewHolder) {
                populateItemRows((ItemViewHolder) viewHolder, position);
            } else if (viewHolder instanceof ItemViewHolder1){
                populateItemRows1((ItemViewHolder1) viewHolder, position);
            }

        }

        @Override
        public int getItemCount() {
        if (dataFragment==null){
            return 0;
        }else{
            return dataFragment.getCategories().size();
        }

        }

        /**
         * The following method decides the type of ViewHolder to display in the RecyclerView
         *
         * @param position
         * @return
         */
        @Override
        public int getItemViewType(int position) {
            if ((position%2)==0){
                return VIEW_TYPE_ITEM;
            }else {
                return VIEW_TYPE_ITEM1;
            }

        }


        private class ItemViewHolder extends RecyclerView.ViewHolder {
           RecyclerView data;
            LinearLayoutManager layoutManager;
            TextView catezgoryName;
            public ItemViewHolder(@NonNull View itemView) {
                super(itemView);
                catezgoryName=itemView.findViewById(R.id.categoryName);
                data=itemView.findViewById(R.id.data);
                data.setHasFixedSize(true);
                layoutManager=new LinearLayoutManager(context);
                RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 1);
                data.setLayoutManager(verticalLayoutManager);
            }
        }
    private class ItemViewHolder1 extends RecyclerView.ViewHolder {
        RecyclerView data;
        LinearLayoutManager layoutManager;
        TextView catezgoryName;
        public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            catezgoryName=itemView.findViewById(R.id.categoryName);
            data=itemView.findViewById(R.id.data);
            data.setHasFixedSize(true);
            layoutManager=new LinearLayoutManager(context);
            RecyclerView.LayoutManager verticalLayoutManager = new GridLayoutManager(context, 2);
            data.setLayoutManager(verticalLayoutManager);

        }
    }




        private void populateItemRows(ItemViewHolder viewHolder, int position) {
            if (dataFragment.getCategories().size()>0) {
                viewHolder.catezgoryName.setText(dataFragment.getCategories().get(position).getCategoryName());
                categoryitem1Adapter = new CategoryItem1Adapter(context, dataFragment.getCategories().get(position).getList());
                viewHolder.data.setAdapter(categoryitem1Adapter);
            }
        }


    private void populateItemRows1(ItemViewHolder1 viewHolder, int position) {
        if (dataFragment.getCategories().size()>0) {
            viewHolder.catezgoryName.setText(dataFragment.getCategories().get(position).getCategoryName());
            categoryitemAdapter = new CategoryItemAdapter(context, dataFragment.getCategories().get(position).getList());
            viewHolder.data.setAdapter(categoryitemAdapter);
        }
    }

    }

