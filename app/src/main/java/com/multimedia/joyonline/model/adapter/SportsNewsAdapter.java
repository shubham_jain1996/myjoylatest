package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.fragmentData.ListFragment;

import java.util.List;


public class SportsNewsAdapter extends RecyclerView.Adapter<SportsNewsAdapter.ViewHolder> {
    Context mContext;
    private int mRowIndex = -1;
   List<ListFragment> newsTitle;


    public SportsNewsAdapter(Context context, List<ListFragment> list) {
        this.mContext = context;
        this.newsTitle = newsTitle;
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }



    @NonNull
    @Override
    public SportsNewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rvsports1item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SportsNewsAdapter.ViewHolder viewHolder, int position) {

              viewHolder.text.setText(newsTitle.get(position).getTitle());
              viewHolder.itemView.setTag(mRowIndex);


    }

    @Override
    public int getItemCount() {
             try {
                 return newsTitle.size();
             }catch (Exception e){
                 return 0;
             }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandPic,lock;
        TextView text;


        public ViewHolder(View itemView) {
            super(itemView);
            brandPic = itemView.findViewById(R.id.image1);
            text=itemView.findViewById(R.id.text1);

        }
    }
}
