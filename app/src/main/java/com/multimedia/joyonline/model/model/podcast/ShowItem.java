package com.multimedia.joyonline.model.model.podcast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShowItem {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("guid")
    @Expose
    private Object guid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("xmlFilename")
    @Expose
    private String xmlFilename;
    @SerializedName("prefixUrl")
    @Expose
    private Object prefixUrl;
    @SerializedName("limit")
    @Expose
    private Object limit;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("rssFeed")
    @Expose
    private String rssFeed;
    @SerializedName("syndications")
    @Expose
    private Syndications syndications;
    @SerializedName("categories")
    @Expose
    private List<String> categories = null;
    @SerializedName("parameters")
    @Expose
    private List<Object> parameters = null;
    @SerializedName("awEpisodeId")
    @Expose
    private Object awEpisodeId;
    @SerializedName("awCollectionId")
    @Expose
    private Object awCollectionId;
    @SerializedName("awGenre")
    @Expose
    private Object awGenre;
    @SerializedName("itunesSubtitle")
    @Expose
    private Object itunesSubtitle;
    @SerializedName("itunesSummary")
    @Expose
    private Object itunesSummary;
    @SerializedName("itunesAuthor")
    @Expose
    private String itunesAuthor;
    @SerializedName("itunesExplicit")
    @Expose
    private String itunesExplicit;
    @SerializedName("itunesBlock")
    @Expose
    private String itunesBlock;
    @SerializedName("itunesName")
    @Expose
    private Object itunesName;
    @SerializedName("itunesEmail")
    @Expose
    private Object itunesEmail;
    @SerializedName("itunesKeywords")
    @Expose
    private Object itunesKeywords;
    @SerializedName("itunesType")
    @Expose
    private Object itunesType;
    @SerializedName("itunesNewFeed")
    @Expose
    private Object itunesNewFeed;
    @SerializedName("itunesLink")
    @Expose
    private Object itunesLink;
    @SerializedName("copyright")
    @Expose
    private Object copyright;
    @SerializedName("googlePlayLink")
    @Expose
    private Object googlePlayLink;
    @SerializedName("subOverrideLink")
    @Expose
    private Object subOverrideLink;
    @SerializedName("ttl")
    @Expose
    private Object ttl;
    @SerializedName("group")
    @Expose
    private Integer group;
    @SerializedName("user")
    @Expose
    private Integer user;
    @SerializedName("createdBy")
    @Expose
    private String createdBy;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("createdAtTimestamp")
    @Expose
    private Integer createdAtTimestamp;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("updatedAtTimestamp")
    @Expose
    private Integer updatedAtTimestamp;
    @SerializedName("backgroundColor")
    @Expose
    private Object backgroundColor;
    @SerializedName("primaryColor")
    @Expose
    private Object primaryColor;
    @SerializedName("lighterColor")
    @Expose
    private Object lighterColor;
    @SerializedName("fontSelect")
    @Expose
    private Object fontSelect;
    @SerializedName("disableScrub")
    @Expose
    private Object disableScrub;
    @SerializedName("disableDownload")
    @Expose
    private Object disableDownload;
    @SerializedName("playerAutoCreation")
    @Expose
    private Integer playerAutoCreation;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getGuid() {
        return guid;
    }

    public void setGuid(Object guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getXmlFilename() {
        return xmlFilename;
    }

    public void setXmlFilename(String xmlFilename) {
        this.xmlFilename = xmlFilename;
    }

    public Object getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(Object prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public Object getLimit() {
        return limit;
    }

    public void setLimit(Object limit) {
        this.limit = limit;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRssFeed() {
        return rssFeed;
    }

    public void setRssFeed(String rssFeed) {
        this.rssFeed = rssFeed;
    }

    public Syndications getSyndications() {
        return syndications;
    }

    public void setSyndications(Syndications syndications) {
        this.syndications = syndications;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void setParameters(List<Object> parameters) {
        this.parameters = parameters;
    }

    public Object getAwEpisodeId() {
        return awEpisodeId;
    }

    public void setAwEpisodeId(Object awEpisodeId) {
        this.awEpisodeId = awEpisodeId;
    }

    public Object getAwCollectionId() {
        return awCollectionId;
    }

    public void setAwCollectionId(Object awCollectionId) {
        this.awCollectionId = awCollectionId;
    }

    public Object getAwGenre() {
        return awGenre;
    }

    public void setAwGenre(Object awGenre) {
        this.awGenre = awGenre;
    }

    public Object getItunesSubtitle() {
        return itunesSubtitle;
    }

    public void setItunesSubtitle(Object itunesSubtitle) {
        this.itunesSubtitle = itunesSubtitle;
    }

    public Object getItunesSummary() {
        return itunesSummary;
    }

    public void setItunesSummary(Object itunesSummary) {
        this.itunesSummary = itunesSummary;
    }

    public String getItunesAuthor() {
        return itunesAuthor;
    }

    public void setItunesAuthor(String itunesAuthor) {
        this.itunesAuthor = itunesAuthor;
    }

    public String getItunesExplicit() {
        return itunesExplicit;
    }

    public void setItunesExplicit(String itunesExplicit) {
        this.itunesExplicit = itunesExplicit;
    }

    public String getItunesBlock() {
        return itunesBlock;
    }

    public void setItunesBlock(String itunesBlock) {
        this.itunesBlock = itunesBlock;
    }

    public Object getItunesName() {
        return itunesName;
    }

    public void setItunesName(Object itunesName) {
        this.itunesName = itunesName;
    }

    public Object getItunesEmail() {
        return itunesEmail;
    }

    public void setItunesEmail(Object itunesEmail) {
        this.itunesEmail = itunesEmail;
    }

    public Object getItunesKeywords() {
        return itunesKeywords;
    }

    public void setItunesKeywords(Object itunesKeywords) {
        this.itunesKeywords = itunesKeywords;
    }

    public Object getItunesType() {
        return itunesType;
    }

    public void setItunesType(Object itunesType) {
        this.itunesType = itunesType;
    }

    public Object getItunesNewFeed() {
        return itunesNewFeed;
    }

    public void setItunesNewFeed(Object itunesNewFeed) {
        this.itunesNewFeed = itunesNewFeed;
    }

    public Object getItunesLink() {
        return itunesLink;
    }

    public void setItunesLink(Object itunesLink) {
        this.itunesLink = itunesLink;
    }

    public Object getCopyright() {
        return copyright;
    }

    public void setCopyright(Object copyright) {
        this.copyright = copyright;
    }

    public Object getGooglePlayLink() {
        return googlePlayLink;
    }

    public void setGooglePlayLink(Object googlePlayLink) {
        this.googlePlayLink = googlePlayLink;
    }

    public Object getSubOverrideLink() {
        return subOverrideLink;
    }

    public void setSubOverrideLink(Object subOverrideLink) {
        this.subOverrideLink = subOverrideLink;
    }

    public Object getTtl() {
        return ttl;
    }

    public void setTtl(Object ttl) {
        this.ttl = ttl;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getUser() {
        return user;
    }

    public void setUser(Integer user) {
        this.user = user;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedAtTimestamp() {
        return createdAtTimestamp;
    }

    public void setCreatedAtTimestamp(Integer createdAtTimestamp) {
        this.createdAtTimestamp = createdAtTimestamp;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedAtTimestamp() {
        return updatedAtTimestamp;
    }

    public void setUpdatedAtTimestamp(Integer updatedAtTimestamp) {
        this.updatedAtTimestamp = updatedAtTimestamp;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getPrimaryColor() {
        return primaryColor;
    }

    public void setPrimaryColor(Object primaryColor) {
        this.primaryColor = primaryColor;
    }

    public Object getLighterColor() {
        return lighterColor;
    }

    public void setLighterColor(Object lighterColor) {
        this.lighterColor = lighterColor;
    }

    public Object getFontSelect() {
        return fontSelect;
    }

    public void setFontSelect(Object fontSelect) {
        this.fontSelect = fontSelect;
    }

    public Object getDisableScrub() {
        return disableScrub;
    }

    public void setDisableScrub(Object disableScrub) {
        this.disableScrub = disableScrub;
    }

    public Object getDisableDownload() {
        return disableDownload;
    }

    public void setDisableDownload(Object disableDownload) {
        this.disableDownload = disableDownload;
    }

    public Integer getPlayerAutoCreation() {
        return playerAutoCreation;
    }

    public void setPlayerAutoCreation(Integer playerAutoCreation) {
        this.playerAutoCreation = playerAutoCreation;
    }
}
