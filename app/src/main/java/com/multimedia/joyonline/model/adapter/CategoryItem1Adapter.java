package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.multimedia.joyonline.R;

import java.util.List;


public class CategoryItem1Adapter extends RecyclerView.Adapter<CategoryItem1Adapter.ViewHolder> {
    Context mContext;
    private int mRowIndex = -1;
    List<com.multimedia.joyonline.model.model.homeModel.List> newsTitle;

    public CategoryItem1Adapter(Context context, List<com.multimedia.joyonline.model.model.homeModel.List> newsTitle) {
        this.mContext = context;
        this.newsTitle = newsTitle;
    }

    public void setRowIndex(int index) {
        mRowIndex = index;
    }



    @NonNull
    @Override
    public CategoryItem1Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.rv1item, viewGroup, false);
        return new CategoryItem1Adapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryItem1Adapter.ViewHolder viewHolder, int position) {

        viewHolder.text.setText(newsTitle.get(position).getTitle());
        Glide.with(mContext).load("http://photos.myjoyonline.com/photos/news/"+newsTitle.get(position).getImageUrl()).into(viewHolder.brandPic);
        viewHolder.itemView.setTag(mRowIndex);


    }

    @Override
    public int getItemCount() {
        try {
            return newsTitle.size();
        }catch (Exception e){
            return 0;
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView brandPic,lock;
        TextView text;


        public ViewHolder(View itemView) {
            super(itemView);
            brandPic = itemView.findViewById(R.id.image1);
            text=itemView.findViewById(R.id.text1);

        }
    }
}
