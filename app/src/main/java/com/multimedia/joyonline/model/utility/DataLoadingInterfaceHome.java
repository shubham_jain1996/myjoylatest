package com.multimedia.joyonline.model.utility;

import com.multimedia.joyonline.model.model.homeModel.List;
import com.multimedia.joyonline.model.model.youtubeFiles.HomeRecyclerModel;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;

public interface DataLoadingInterfaceHome {
    void preExecute();
    void postExecute(HomeRecyclerModel homeClasses);
}
