package com.multimedia.joyonline.model.model.homeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SideMenuList {
    @SerializedName("Title")
    @Expose
    private String Title;



    @SerializedName("Icon")
    @Expose
    private Integer icon;

    @SerializedName("Id")
    @Expose
    private Integer id;

    @SerializedName("isPlay")
    @Expose
    private Boolean isSelected;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public SideMenuList() {
    }

    public SideMenuList(String title, Integer icon, Integer id, Boolean isSelected) {
        Title = title;
        this.icon = icon;
        this.id = id;
        this.isSelected = isSelected;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getisSelected() {
        return isSelected;
    }

    public void setisSelected(Boolean isPlay) {
        this.isSelected = isPlay;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }





}
