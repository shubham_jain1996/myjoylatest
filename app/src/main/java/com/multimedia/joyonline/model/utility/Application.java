package com.multimedia.joyonline.model.utility;

import android.content.Context;
import android.content.SharedPreferences;


public class Application extends android.app.Application{
    public static SharedPreferences shardPref;
    private static Context mContext;


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        shardPref = getSharedPreferences(Constants.PREF_NAME,MODE_PRIVATE);
       // MobileAds.initialize(this, this.getResources().getString(R.string.appId));


    }
    public static Context getContext() {
        return mContext;
    }
}
