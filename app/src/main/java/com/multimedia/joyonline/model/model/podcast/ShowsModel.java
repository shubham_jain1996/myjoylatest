package com.multimedia.joyonline.model.model.podcast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShowsModel {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("guid")
    @Expose
    private String guid;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("group")
    @Expose
    private Integer group;
    @SerializedName("users")
    @Expose
    private List<Integer> users = null;
    @SerializedName("items")
    @Expose
    private List<ShowItem> items = null;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("createdAtTimestamp")
    @Expose
    private Integer createdAtTimestamp;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("updatedAtTimestamp")
    @Expose
    private Integer updatedAtTimestamp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public List<Integer> getUsers() {
        return users;
    }

    public void setUsers(List<Integer> users) {
        this.users = users;
    }

    public List<ShowItem> getItems() {
        return items;
    }

    public void setItems(List<ShowItem> items) {
        this.items = items;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getCreatedAtTimestamp() {
        return createdAtTimestamp;
    }

    public void setCreatedAtTimestamp(Integer createdAtTimestamp) {
        this.createdAtTimestamp = createdAtTimestamp;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getUpdatedAtTimestamp() {
        return updatedAtTimestamp;
    }

    public void setUpdatedAtTimestamp(Integer updatedAtTimestamp) {
        this.updatedAtTimestamp = updatedAtTimestamp;
    }
}
