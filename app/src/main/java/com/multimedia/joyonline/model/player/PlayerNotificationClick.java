package com.multimedia.joyonline.model.player;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.multimedia.joyonline.model.utility.Constants;

public class PlayerNotificationClick extends BroadcastReceiver  {

    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()){
            case Constants.ACTION.PLAY_ACTION:
                context.sendBroadcast(new Intent(Constants.ACTION.PLAY_ACTION));
                break;
            case Constants.ACTION.PRIVIOUS_ACTION:
                context.sendBroadcast(new Intent(Constants.ACTION.PRIVIOUS_ACTION));
                break;
            case Constants.ACTION.FORWORD_ACTION:
                context.sendBroadcast(new Intent(Constants.ACTION.FORWORD_ACTION));
                break;

        }


    }
}
