package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.homeModel.Data;


public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    Context mContext;
    private int mRowIndex = -1;
   Data data;

         public MainAdapter(Context context, Data data) {
        this.mContext = context;
       this.data = data;
    }
    public void setRowIndex(int index) {
        mRowIndex = index;
    }



    @NonNull
    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout1, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MainAdapter.ViewHolder viewHolder, int position) {
           /*  viewHolder.text.setText(newsTitle.get(position));
             viewHolder.itemView.setTag(mRowIndex);
        fragmentDataAdapter adapter = new fragmentDataAdapter(mContext, );
        adapter.setRowIndex(i);
        mv.image.setAdapter(adapter);*/


    }

    @Override
    public int getItemCount() {
             try {
                 return 0;
             }catch (Exception e){
                 return 0;
             }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        RecyclerView homeData;


        public ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
          //  homeData=itemView.findViewById(R.id.storieNews);
          //  RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(title.getContext(),LinearLayoutManager.VERTICAL,false);
            homeData.setHasFixedSize(true);
          //  homeData.setLayoutManager(mLayoutManager);
            ViewCompat.setNestedScrollingEnabled(homeData,false);

        }
    }
}
