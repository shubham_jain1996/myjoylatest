package com.multimedia.joyonline.model.adapter;


/**
 * Created by h p on 12/29/2018.
 */

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


import com.multimedia.joyonline.model.model.homeModel.SideMenuList;
import com.multimedia.joyonline.view.mainActivity.fragment.Home;
import com.multimedia.joyonline.view.mainActivity.fragment.registerFragment.YouTubePlaylists;

import java.util.ArrayList;
import java.util.List;

public  class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<SideMenuList> mFragmentTagList = new ArrayList<>();
    private FragmentManager mFragmentManager;
    private Fragment mFragmentAtPos0;

    public ViewPagerAdapter(FragmentManager manager, List<SideMenuList> mFragmentTagList) {
        super(manager);
        this.mFragmentTagList= mFragmentTagList;
        mFragmentManager = manager;
    }
    @Override
    public int getItemPosition(Object object) {

            return POSITION_NONE;

    }
    @Override
    public Fragment getItem(int position) {
        return new Home();
    }

    @Override
    public int getCount() {
        return mFragmentTagList.size();
    }

    public void addFragment(Fragment fragment, String title,String tag) {
     //   mFragmentList.add(new Home());
     //   mFragmentTitleList.add(title);
      //  mFragmentTagList.add(tag);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTagList.get(position).getTitle();
    }
}
