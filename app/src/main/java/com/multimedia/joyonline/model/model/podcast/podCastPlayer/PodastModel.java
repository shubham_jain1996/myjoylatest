package com.multimedia.joyonline.model.model.podcast.podCastPlayer;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PodastModel {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("enclosure")
    @Expose
    private Enclosure enclosure;
    @SerializedName("guid")
    @Expose
    private Guid guid;
    @SerializedName("itunes:author")
    @Expose
    private Object itunesAuthor;
    @SerializedName("itunes:explicit")
    @Expose
    private String itunesExplicit;
    @SerializedName("itunes:block")
    @Expose
    private Object itunesBlock;
    @SerializedName("itunes:duration")
    @Expose
    private Object itunesDuration;
    @SerializedName("itunes:summary")
    @Expose
    private Object itunesSummary;
    @SerializedName("itunes:season")
    @Expose
    private Object itunesSeason;
    @SerializedName("itunes:episode")
    @Expose
    private Object itunesEpisode;
    @SerializedName("itunes:episodeType")
    @Expose
    private Object itunesEpisodeType;
    @SerializedName("itunes:title")
    @Expose
    private Object itunesTitle;
    @SerializedName("pubDate")
    @Expose
    private String pubDate;
    @SerializedName("pubDateTimestamp")
    @Expose
    private Integer pubDateTimestamp;


    private Boolean isplay=false;
    private Boolean isPause=false;

    public Boolean getPause() {
        return isPause;
    }

    public void setPause(Boolean pause) {
        isPause = pause;
    }

    public Boolean getIsplay() {
        return isplay;
    }

    public void setIsplay(Boolean isplay) {
        this.isplay = isplay;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public Guid getGuid() {
        return guid;
    }

    public void setGuid(Guid guid) {
        this.guid = guid;
    }

    public Object getItunesAuthor() {
        return itunesAuthor;
    }

    public void setItunesAuthor(Object itunesAuthor) {
        this.itunesAuthor = itunesAuthor;
    }

    public String getItunesExplicit() {
        return itunesExplicit;
    }

    public void setItunesExplicit(String itunesExplicit) {
        this.itunesExplicit = itunesExplicit;
    }

    public Object getItunesBlock() {
        return itunesBlock;
    }

    public void setItunesBlock(Object itunesBlock) {
        this.itunesBlock = itunesBlock;
    }

    public Object getItunesDuration() {
        return itunesDuration;
    }

    public void setItunesDuration(Object itunesDuration) {
        this.itunesDuration = itunesDuration;
    }

    public Object getItunesSummary() {
        return itunesSummary;
    }

    public void setItunesSummary(Object itunesSummary) {
        this.itunesSummary = itunesSummary;
    }

    public Object getItunesSeason() {
        return itunesSeason;
    }

    public void setItunesSeason(Object itunesSeason) {
        this.itunesSeason = itunesSeason;
    }

    public Object getItunesEpisode() {
        return itunesEpisode;
    }

    public void setItunesEpisode(Object itunesEpisode) {
        this.itunesEpisode = itunesEpisode;
    }

    public Object getItunesEpisodeType() {
        return itunesEpisodeType;
    }

    public void setItunesEpisodeType(Object itunesEpisodeType) {
        this.itunesEpisodeType = itunesEpisodeType;
    }

    public Object getItunesTitle() {
        return itunesTitle;
    }

    public void setItunesTitle(Object itunesTitle) {
        this.itunesTitle = itunesTitle;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public Integer getPubDateTimestamp() {
        return pubDateTimestamp;
    }

    public void setPubDateTimestamp(Integer pubDateTimestamp) {
        this.pubDateTimestamp = pubDateTimestamp;
    }

}
