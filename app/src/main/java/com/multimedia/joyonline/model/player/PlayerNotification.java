package com.multimedia.joyonline.model.player;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.RemoteViews;

import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.RadioModel;
import com.multimedia.joyonline.model.utility.Application;
import com.multimedia.joyonline.model.utility.Constants;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.PlayerActivity;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by h p on 1/17/2019.
 */

public class PlayerNotification {

    public static NotificationCompat.Builder builder;
    private static final String CHANNEL_ID = "media_playback_Myjoy";
    public static PlaybackStateCompat.Builder mStateBuilder;
    private static  NotificationManager mNotificationManager;
    public static MediaSessionCompat mMediaSession;
    private static MediaSessionCompat.Token token;
    String play_pause;
    static int position;
    static List<RadioModel> channelList1= new ArrayList<>();
    static List<RadioModel> radioModel= new ArrayList<>();

    static setUpNotification setUpNotification;

    public static void setupView() {

        //  mMediaSession = new MediaSessionCompat(this, this.getClass().getSimpleName());
        mMediaSession = new MediaSessionCompat(Application.getContext(),PlayerNotification.class.getSimpleName());
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        // Do not let MediaButtons restart the player when the app is not visible.
        mMediaSession.setMediaButtonReceiver(null);

        mStateBuilder = new PlaybackStateCompat.Builder().setActions(
                PlaybackStateCompat.ACTION_PLAY |
                        PlaybackStateCompat.ACTION_PAUSE |
                        PlaybackStateCompat.ACTION_PLAY_PAUSE |
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT |
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS);

        mMediaSession.setPlaybackState(mStateBuilder.build());
        mMediaSession.setCallback(new MySessionCallback());
        mMediaSession.setActive(true);
    }

    private static class MySessionCallback extends MediaSessionCompat.Callback {
        @Override
        public void onPlay() {
            SharedExoPlayer.getInstance().checkAudio();

        }

        @Override
        public void onFastForward() {
            super.onFastForward();
        }

        @Override
        public void onPause()
        {
            SharedExoPlayer.getInstance().pauseRadio();
        }

        @Override
        public void onSkipToPrevious() {
            if (position<=0){
                return;
            }else{
                position--;
                setUpNotification.manageData(channelList1,position,radioModel);

            }
          /*  long time=SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition();
            time=time-10000;
            if (time>0.00) {

                SharedExoPlayer.getInstance().exoPlayer.seekTo(time);
            }*/
        }


        @Override
        public void onSkipToNext() {
            if (position>=channelList1.size()){
                return;
            }else{
                    position++;

                if (position<channelList1.size()){
                    setUpNotification.manageData(channelList1,position,radioModel);
                }
            }
          /*  long time=SharedExoPlayer.getInstance().exoPlayer.getCurrentPosition();
            time=time+10000;
            if (SharedExoPlayer.getInstance().exoPlayer.getDuration()>time) {
                SharedExoPlayer.getInstance().exoPlayer.seekTo(time);
            }*/
        }
    }

    public static void showNotification(PlaybackStateCompat state, int songPosition, List<RadioModel> radioData) {
        channelList1=radioData;
        position=songPosition;
        radioModel=radioData;


        // You only need to create the channel on API 26+ devices
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createChannel();
        }
        RemoteViews notificationView = new RemoteViews(Application.getContext().getPackageName(), R.layout.notificatio_player_collapsed);
        RemoteViews notificationViewExpand = new RemoteViews(Application.getContext().getPackageName(), R.layout.notificatio_player);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(Application.getContext(), CHANNEL_ID);

        notificationView.setTextViewText(R.id.fmName,radioModel.get(position).getTitle());
        notificationView.setImageViewResource(R.id.playerImage,radioModel.get(position).getIcon());

        notificationViewExpand.setTextViewText(R.id.channelName,radioModel.get(position).getTitle());
        notificationViewExpand.setTextViewText(R.id.tagLine,radioModel.get(position).getTagLine());
        notificationViewExpand.setImageViewResource(R.id.playerImage,radioModel.get(position).getIcon());






        /*int icon;
        String play_pause;
        if (state.getState() == PlaybackStateCompat.STATE_PLAYING) {
            icon = R.drawable.exo_controls_pause;
            play_pause = Application.getContext().getString(R.string.pause);
        }
        else {
            icon = R.drawable.exo_controls_play;
            play_pause = Application.getContext().getString(R.string.play);
        }

        NotificationCompat.Action playPauseAction = new NotificationCompat.Action(
                icon, play_pause,
                MediaButtonReceiver.buildMediaButtonPendingIntent(Application.getContext(),
                        PlaybackStateCompat.ACTION_PLAY_PAUSE));

        NotificationCompat.Action restartAction = new NotificationCompat.Action(
                R.drawable.ic_rewind, Application.getContext().getString(R.string.restart),
                MediaButtonReceiver.buildMediaButtonPendingIntent(Application.getContext(),
                        PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS));

        NotificationCompat.Action NextAction = new NotificationCompat.Action(
                R.drawable.ic_fast_forward, Application.getContext().getString(R.string.next),
                MediaButtonReceiver.buildMediaButtonPendingIntent(Application.getContext(),
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT));*/


        Intent play = new Intent(Application.getContext(), PlayerNotificationClick.class);
        play.setAction(Constants.ACTION.PLAY_ACTION);
        PendingIntent pplay = PendingIntent.getBroadcast(Application.getContext(), 0,
                play, PendingIntent.FLAG_CANCEL_CURRENT);



        Intent rewind = new Intent(Application.getContext(), PlayerNotificationClick.class);
        rewind.setAction(Constants.ACTION.PRIVIOUS_ACTION);
        PendingIntent prewind = PendingIntent.getBroadcast(Application.getContext(), 2,
                rewind, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent forword = new Intent(Application.getContext(), PlayerNotificationClick.class);
        forword.setAction(Constants.ACTION.FORWORD_ACTION);
        PendingIntent pforword = PendingIntent.getBroadcast(Application.getContext(), 3,
                forword, PendingIntent.FLAG_CANCEL_CURRENT);
        if (state.getState() == PlaybackStateCompat.STATE_PLAYING) {
           notificationView.setImageViewResource(R.id.play,R.drawable.exo_icon_pause);
            notificationViewExpand.setImageViewResource(R.id.play,R.drawable.ic_pause);
        }
        else {
            notificationView.setImageViewResource(R.id.play,R.drawable.exo_icon_play);
            notificationViewExpand.setImageViewResource(R.id.play,R.drawable.ic_play);
        }
        notificationView.setOnClickPendingIntent(R.id.play,pplay);
        notificationView.setOnClickPendingIntent(R.id.forword,pforword);
        notificationView.setOnClickPendingIntent(R.id.rewind,prewind);

        notificationViewExpand.setOnClickPendingIntent(R.id.play,pplay);
        notificationViewExpand.setOnClickPendingIntent(R.id.forword,pforword);
        notificationViewExpand.setOnClickPendingIntent(R.id.rewind,prewind);



        Intent notificationIntent = new Intent(Application.getContext(), MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentPendingIntent = PendingIntent.getActivity(Application.getContext(), 0,
                notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        token = mMediaSession.getSessionToken();

       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setSmallIcon(R.drawable.ic_stat_padmasana_2);
        } else {
            builder.setSmallIcon(R.drawable.iqbal_icon);
        }*/
        builder.setContentTitle(SharedExoPlayer.getInstance().audio_name)
                // .setContentText(ApplicationContext.getContext().getString(R.string.notification_text))
                .setContentIntent(contentPendingIntent)
                .setCustomContentView(notificationView)
                .setCustomBigContentView(notificationViewExpand)
                //.setAutoCancel(true)
                // .setColor(Color.RED)
                .setSmallIcon(notiSmallIcon())
                .setContentIntent(pforword)
                .setContentIntent(pplay)
                .setContentIntent(prewind)

                .setStyle(new NotificationCompat.BigTextStyle()
                        );
        mNotificationManager = (NotificationManager) Application.getContext().getSystemService(NOTIFICATION_SERVICE);
        mNotificationManager.notify(105, builder.build());
    }




    /**
     * The NotificationCompat class does not create a channel for you. You still have to create a channel yourself
     */

    @RequiresApi(Build.VERSION_CODES.O)
    private static void createChannel() {
        NotificationManager mNotificationManager = (NotificationManager) Application.getContext().getSystemService(NOTIFICATION_SERVICE);
        // The id of the channel.
        String id = CHANNEL_ID;
        // The user-visible name of the channel.
        CharSequence name = "Media playback";
        // The user-visible description of the channel.
        String description = "Media playback controls";
        int importance = NotificationManager.IMPORTANCE_LOW;
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.setShowBadge(false);
        mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        mNotificationManager.createNotificationChannel(mChannel);
    }

    public static int notiSmallIcon()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return R.drawable.myjoylogo;
        }
        else {
            return R.drawable.myjoylogo;
        }
    }

    private static void manageModelData(int position1) {
        if (setUpNotification!=null){
            Log.d("notificationStatus", "manageModelData: not null");
        }else{
            Log.d("notificationStatus", "manageModelData:  null");
        }

        for (int i = 0; i<channelList1.size(); i++){
            if (i==position1){
                channelList1.get(i).setIsPlay(true);
            }else{
                channelList1.get(i).setIsPlay(false);
            }
        }


    }

    public interface setUpNotification{
        void manageData(List<RadioModel> url, int position, List<RadioModel> radioData);
    }

}
