package com.multimedia.joyonline.model.model.homeModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {
    public Category(Integer categoryId, String categoryName, String categoryUrl) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.categoryUrl = categoryUrl;
    }

    public Category() {
    }

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("category_url")
    @Expose
    private String categoryUrl;

    @SerializedName("more_stories")
    @Expose
    private String moreStories;
    @SerializedName("list")
    @Expose
    private java.util.List<List> list = null;




    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryUrl() {
        return categoryUrl;
    }

    public void setCategoryUrl(String categoryUrl) {
        this.categoryUrl = categoryUrl;
    }


    public String getMoreStories() {
        return moreStories;
    }

    public void setMoreStories(String moreStories) {
        this.moreStories = moreStories;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }
}
