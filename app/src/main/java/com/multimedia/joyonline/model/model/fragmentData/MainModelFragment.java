package com.multimedia.joyonline.model.model.fragmentData;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainModelFragment implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private DataFragment data;
    public final static Parcelable.Creator<MainModelFragment> CREATOR = new Creator<MainModelFragment>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MainModelFragment createFromParcel(Parcel in) {
            return new MainModelFragment(in);
        }

        public MainModelFragment[] newArray(int size) {
            return (new MainModelFragment[size]);
        }

    }
            ;

    protected MainModelFragment(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.data = ((DataFragment) in.readValue((DataFragment.class.getClassLoader())));
    }

    public MainModelFragment() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public DataFragment getData() {
        return data;
    }

    public void setData(DataFragment data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(data);
    }

    public int describeContents() {
        return 0;
    }
}
