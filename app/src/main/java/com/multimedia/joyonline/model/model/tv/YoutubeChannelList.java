package com.multimedia.joyonline.model.model.tv;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YoutubeChannelList implements Parcelable {

    @SerializedName("items")
    @Expose
    private List<ChannelItem> items = null;


    protected YoutubeChannelList(Parcel in) {

        items = in.createTypedArrayList(ChannelItem.CREATOR);
    }

    public static final Creator<YoutubeChannelList> CREATOR = new Creator<YoutubeChannelList>() {
        @Override
        public YoutubeChannelList createFromParcel(Parcel in) {
            return new YoutubeChannelList(in);
        }

        @Override
        public YoutubeChannelList[] newArray(int size) {
            return new YoutubeChannelList[size];
        }
    };



    public List<ChannelItem> getItems() {
        return items;
    }

    public void setItems(List<ChannelItem> items) {
        this.items = items;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeTypedList(items);
    }
}
