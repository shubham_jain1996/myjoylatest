package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.youtubeFiles.HomeRecyclerModel;
import com.multimedia.joyonline.model.model.youtubeFiles.PlayListModel;
import com.multimedia.joyonline.view.mainActivity.MainActivity;
import com.multimedia.joyonline.view.mainActivity.YouTubeVideos;

import java.util.ArrayList;
import java.util.List;

public class HomeDataAdapter extends RecyclerView.Adapter {

        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_ITEM1 = 1;

    private YouTubeHomeAdapter sportsNewAdapter;
    private  StoriesNewsAdapter storiesnewsAdapter;
    Context context;
    List<PlayListModel> allData= new ArrayList<>();
    private Slidingnews_Adapter slidingAdapter;
    private LinearLayoutManager mLayoutManager;

    public HomeDataAdapter(Context context, List<PlayListModel> allData) {
        this.context=context;
        this.allData=allData;
    }


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
               /* View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout1, parent, false);
                return new ItemViewHolder(view);*/

                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout1, parent, false);
                return new ItemViewHolder(view);
            /*else  {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.youtube_fragment, parent, false);
                return new ItemViewHolder1(view);
            }
*/
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
            if (viewHolder instanceof ItemViewHolder) {
                ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
                itemViewHolder.text.setText(allData.get(position).getTitle());
                populateItemRows((ItemViewHolder) viewHolder, position);
                sportsNewAdapter = new YouTubeHomeAdapter(context, allData.get(position).getTopStories());
                itemViewHolder.data.setAdapter(sportsNewAdapter);
                if (allData.get(position).getTopStories()!=null){
                    itemViewHolder.itemView.setVisibility(View.VISIBLE);
                }else {
                    itemViewHolder.itemView.setVisibility(View.INVISIBLE);
                }
                itemViewHolder.moreStories.setOnClickListener(new ViewMoreAction(position));
            }/*else if (viewHolder instanceof ItemViewHolder1){
                ItemViewHolder1 itemViewHolder = (ItemViewHolder1) viewHolder;
               // itemViewHolder.text.setText(allData.get(position).getCategory_Name());
                populateItemRows1((ItemViewHolder1) viewHolder, position);


            }*/

        }

    class ViewMoreAction implements  View.OnClickListener{
        int position = 0;
        ViewMoreAction(Integer position){
            this.position = position;
        }
        @Override
        public void onClick(View v) {
            MainActivity.adCounter+=1;
            Intent i = new Intent(context, YouTubeVideos.class);
            i.putExtra("playlistId",allData.get(position).getPlayListId());
            i.putExtra("type","more");
            context.startActivity(i);
        }
     }
        @Override
        public int getItemCount() {
            return allData.size();
        }


        @Override
        public int getItemViewType(int position) {
            if (position==0){
                return VIEW_TYPE_ITEM1;
            }else {
                return VIEW_TYPE_ITEM;
            }

        }


        private class ItemViewHolder extends RecyclerView.ViewHolder {
           RecyclerView data;
            TextView text;
            TextView moreStories;
            public ItemViewHolder(@NonNull View itemView) {
                super(itemView);
                itemView.setTag(0);
                moreStories=itemView.findViewById(R.id.moreStories);
                text=itemView.findViewById(R.id.title);
                data=itemView.findViewById(R.id.data);
            }
        }

    /*private class ItemViewHolder1 extends RecyclerView.ViewHolder implements YouTubePlayer.OnInitializedListener   {
        YouTubePlayer youTubePlayer;
        boolean wasRestored;
        private String API_KEY="AIzaSyAmldoK5FMhNKOV410GfHVwJjH2qjomISU";

        *//*public ItemViewHolder1(@NonNull View itemView) {
            super(itemView);
            itemView.setTag(1);
            YouTubePlayerSupportFragment frag =(YouTubePlayerSupportFragment)((MainActivity)context).getSupportFragmentManager().findFragmentById(R.id.youtube_fragment1);
            frag.initialize(API_KEY, this);

        }*//*

        @Override
        public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
            this.youTubePlayer=youTubePlayer;
            this.wasRestored=wasRestored;

        }

        @Override
        public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

        }
    }
*/
        private void populateItemRows(ItemViewHolder viewHolder, int i) {
            viewHolder.data.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
            viewHolder.data.setLayoutManager(mLayoutManager);
        }


/*    private void populateItemRows1(ItemViewHolder1 viewHolder, int position) {

    }*/

}

