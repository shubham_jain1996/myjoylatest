package com.multimedia.joyonline.model.adapter;

import android.content.Context;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.multimedia.joyonline.R;
import com.multimedia.joyonline.model.model.homeModel.LatestNews;


import java.util.List;

public class Slidingnews_Adapter extends PagerAdapter {


    private List<LatestNews> latestNewsData;

    private LayoutInflater inflater;
    private Context context;


    public Slidingnews_Adapter(Context context, List<LatestNews> latestNewsData) {
        this.context = context;
        this.latestNewsData=latestNewsData;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return latestNewsData.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.slidingnews_layout, view, false);

        assert imageLayout != null;
        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.background);
        final TextView textView= imageLayout.findViewById(R.id.description);
        final TextView textView1= imageLayout.findViewById(R.id.heading);
        Glide.with(context).load("http://photos.myjoyonline.com/photos/news/"+latestNewsData.get(position).getImageUrl()).into(imageView);
        textView.setText(latestNewsData.get(position).getTitle());
        textView1.setText(latestNewsData.get(position).getImageCaption());
        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }
}
