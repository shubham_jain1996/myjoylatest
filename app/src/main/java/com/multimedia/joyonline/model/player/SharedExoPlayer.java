package com.multimedia.joyonline.model.player;

import android.content.Context;
import android.media.AudioManager;
import android.net.Uri;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.multimedia.joyonline.model.utility.Application;


public class SharedExoPlayer {
    Context context;
    static SharedExoPlayer myExoPlayer;
    public SimpleExoPlayer exoPlayer = null;
    MediaSource audioSource;
    public int songPosition;
    public String audio_name;
    public  String imageUrl;
    String audio_id;
    String audio_url;
     DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(Application.getContext(), Util.getUserAgent(Application.getContext(), "exoplayer2example"), null);
    ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
    SharedExoPlayerListner sharedPlayerListener;
    boolean playerState= false;
    AudioManager am=null;

    String TAG = "CommonExoPlayer";


    public static SharedExoPlayer getInstance() {

        if (myExoPlayer == null) {
            myExoPlayer = new SharedExoPlayer();

        }

        return myExoPlayer;
    }
    public void addItemListner(SharedExoPlayerListner sharedPlayerListener){
        this.sharedPlayerListener = sharedPlayerListener;
    }

        public  void pauseRadio()
        {
            if(exoPlayer!= null) {
                exoPlayer.setPlayWhenReady(false);
            }
        }
        //chanchal
    public  void startRadio()
    {
        if(exoPlayer!= null) {
            exoPlayer.setPlayWhenReady(true);
        }
    }

    public void endRadio(){
        if (exoPlayer!=null){
            exoPlayer.setPlayWhenReady(false);
            exoPlayer=null;


        }
    }
    //chanchal end
    private Player.EventListener eventListener = new Player.EventListener() {

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
          //  Log.i(TAG, "onTimelineChanged");
            if (sharedPlayerListener != null) {
                sharedPlayerListener.onTimelineChanged(timeline,manifest);
            }
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
           // Log.i(TAG, "onTracksChanged");
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
            if (sharedPlayerListener != null) {
                sharedPlayerListener.onLoadingChanged(isLoading);
            }

        }

        @Override
        public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
            if (sharedPlayerListener != null) {
                //set listener on music player
                sharedPlayerListener.onPlayerStateChanged(playWhenReady,playbackState);
            }
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
            if (sharedPlayerListener != null) {
                sharedPlayerListener.onPlayerError(error);
            }
        }

    };

    void prepareExoPlayerFromURL(Uri uri, String title) {
        audioSource = new ExtractorMediaSource.Factory(dataSourceFactory).createMediaSource(uri);
        if (exoPlayer == null) {
            TrackSelector trackSelector = new DefaultTrackSelector();
            exoPlayer = ExoPlayerFactory.newSimpleInstance(Application.getContext(), trackSelector);
            exoPlayer.addListener(eventListener);
        }
        exoPlayer.prepare(audioSource);
        exoPlayer.setPlayWhenReady(true);
    }

    public void PlayAudio(String audio_url1, String AdioName, String imageUrl) {
        audio_name = AdioName;
        audio_url = audio_url1;
        this.imageUrl=imageUrl;
        prepareExoPlayerFromURL(Uri.parse(audio_url), audio_name);
    }

    public interface SharedExoPlayerListner {
        void onPlayerStateChanged(boolean playWhenReady, int playbackState);
        void onLoadingChanged(boolean isLoading);
        void onPlayerError(ExoPlaybackException error);
        void  onTimelineChanged(Timeline timeline, Object manifest);
    }

    public  void  checkAudio()
    {
        am= (AudioManager)Application.getContext().getSystemService(Context.AUDIO_SERVICE);
// Request focus for music stream and pass AudioManager.OnAudioFocusChangeListener
// implementation reference
        int result = am.requestAudioFocus(listener, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
        if(result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
        {
            SharedExoPlayer.getInstance().startRadio();
            // Play
        }



    }

    AudioManager.OnAudioFocusChangeListener listener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange)
        {

            if(SharedExoPlayer.myExoPlayer != null && SharedExoPlayer.getInstance().exoPlayer != null ) {
                if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT) {
                    if (SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady()) {
                        playerState= true;
                    }
                    else
                    {
                        playerState = false;
                    }
                    SharedExoPlayer.getInstance().pauseRadio();
                    // Pause
                }
                else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                    if(playerState) {

                        SharedExoPlayer.getInstance().startRadio();
                    }
                        // Resume
                } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                    if (SharedExoPlayer.getInstance().exoPlayer.getPlayWhenReady()) {
                        playerState = true;
                    }
                    else
                    {playerState = false;}
                    SharedExoPlayer.getInstance().pauseRadio();
                    // Stop or pause depending on your need
                }
                else
                {

                }
            }
        }
    };

}

